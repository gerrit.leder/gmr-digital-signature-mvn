gmr-digital-signature-system V2.0.0
===================================

brought to you by Gerrit Leder, Copyright 2001-2020 (gerrit.leder@gmail.com)

Homepage: https://infinite-taiga-25466.herokuapp.com/
Project repository: https://gitlab.com/gerrit.leder/gmr-digital-signature-mvn

Files:

- $ java -jar gmr-2.0.0.jar: the java executable
- leder9.pk: the public key to my secret private key and the signature...
- gmr-2.0.0.jar.gmr: generated signature, verify with public key "leder9"
- This is the sha512 checksum of leder9.pk:

leder@leder-ryzen-home:~/Projekt/GMR$ sha512sum leder9.pk
3308da991d9428d9f182c3e72faa4b760d38ed6b116de573b0a51631325b08b483e053c0e37d0bd9405fbc3307006c8636abb3c614a9865add454b977cfbed5a leder9.pk

Download gmr-2.0.0.jar for Linux (Virtual) Machine, Windows is now supported, too!

[![BrowserStack Status](https://automate.browserstack.com/badge.svg?badge_key=WTdPOHFiTlpveEN5ZlBjUnk2RzhHbElXVnhHVWU3V25uUitLakZOT1ZUWT0tLXVDT0hKNWNZblYvVk9GYTU5VVhmQVE9PQ==--879480bd912133b8a521b27a1933c1afe0144267)](https://automate.browserstack.com/public-build/WTdPOHFiTlpveEN5ZlBjUnk2RzhHbElXVnhHVWU3V25uUitLakZOT1ZUWT0tLXVDT0hKNWNZblYvVk9GYTU5VVhmQVE9PQ==--879480bd912133b8a521b27a1933c1afe0144267)

