package org.no_ip.leder.gmr;

//Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2014  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


//Dieses Paket beinhaltet die Klassen Signature_Path und Secret_Key und kapselt
//ihre Zugriffe.
//ZUGRIFFE AUF p UND q DES Secret_Key SIND AUF DIESES PAKET BESCHRAENKT!
//Insbesondere duerfen die zusammengesetzten Zahlen n_f und n_g vom Typ Compound
//des Secret_Key nicht ausserhalb (z. B. im Public_Key) des Packets verwendet
//werden.
//package Secret_Cluster;//no package
//ZUGRIFFE AUF p UND q DES Secret_Key SIND AUF DAS LOKALE VERZEICHNIS BESCHR.
//KEINE PERSISTENZ FUER p UND q DURCH transient.

import java.util.BitSet;
import java.util.Vector;
import java.util.Random;
import java.math.BigInteger;
import java.io.*;

public class Signature_Path implements Serializable {
//Speichert den Signaturpfad der letzten Unterschriebenen Nachricht. Diese 
//Information ist oeffentlich.
//Der Secret_Key wird mit geheimer Information angelegt.
//ACHTUNG:Die Zugriffe auf den Secret_Key und auf Random müssen transient sein!


/**
	 * 
	 */

private static final long serialVersionUID = 7814973300149098150L;
//Konstante Zahlen:
final BigInteger zero = new BigInteger ("0");
final BigInteger one = new BigInteger ("1");
final BigInteger two = new BigInteger ("2");
final BigInteger three = new BigInteger ("3");
final BigInteger four = new BigInteger ("4");


//Klassenattribute:
public Vector<Node> path;

//Klassenattribute (GEHEIM):
public transient Secret_Key sk;
transient protected Random std_rnd; //Zufallszahlgenerator

//Klassenattribute (privat):
private int k;//Bitlaenge der Zufallszahlen p, q 
//Klassenattribute (transient):
transient BigInteger message;

//Konstruktor:
public Signature_Path (BigInteger message) {//Done: BigInteger message 
	//Klassenattribute initialisieren:
	path= new Vector<Node>();

	//Klassenattribute initialisieren (GEHEIM):
	sk = new Secret_Key ();
	std_rnd = new Random (); //Zufallszahlgenerator

	//Klassenattribute initialisieren (privat):
	k = 0;//Bitlaenge der Zufallszahlen p, q 
	//Klassenattribute initialisieren (transient):
	this.message = message;
	
}

public Signature_Path (int b, int k, int certainty, Random std_rnd, BigInteger message) {//Done: BigInteger message ) { 
  //Legt Pfad mit vollstaendigen Knoten (Node) im Vektor path an. 
  //Die Laenge des Pfades ist b+2. b ist im oeffentlichen Schluessel festgelegt.
  //Legt den Secret_Key mit geheimer Information an.

  //lokale Variablen:
  D root, tag, child0, child1;
  Vector<BigInteger> children = new Vector<BigInteger> (2);//in children stehen Zahlen D und als letzter Knoten message vom Typ BigInteger
  int next;
  this.message = message;
  Vector<BigInteger> vector = null;
  
  //Variableninitialisierung:
  path = new Vector<Node> (b+3);// war: b+2, war: b+3
  sk = new Secret_Key (k, certainty, std_rnd);
  this.k = k;
  this.std_rnd = std_rnd;
  //Anlegen des ersten Knotens (#0):
  root = new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
  next = 0;
  child0 = new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
  child1 = new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
  children.addElement ( child0.get () );
  children.addElement ( child1.get () );
  tag = Tag_of_Item (sk.n_f, children, root);
  path.addElement (new Node (root, tag, children, next));
  
  for (int i=1; i<=b; i++) {//war: i=2 -> b-1 Elemente erzeugen und in Vector speichern //war: i=1 -> b Elemente
    root = child0;
    next = 0;
    child0 = new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
    child1 = new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
    children = new Vector<BigInteger> (2);
    children.addElement ( child0.get () );
    children.addElement ( child1.get () );
    tag = Tag_of_Item (sk.n_f, children, root);
    path.addElement (new Node (root, tag, children, next));
  }

  //Anlegen des vorletzten Knotens:
    root = child0;
    next = 0;
    child0 = new D (new Number ( sk.n_g.get().toByteArray() ), k, std_rnd);
    children = new Vector<BigInteger> (1);
    children.addElement ( child0.get () );
    tag = Tag_of_Item (sk.n_f, children, root);
    path.addElement (new Node (root, tag, children, next));
  
  //Anlegen des letzten Knotens:
    root = child0;
    next = 0;
    child0 = new D (new Number ( sk.n_g.get().toByteArray() ), k, std_rnd);
    children = new Vector<BigInteger> (1);
    //children.addElement ( child0.get () );//DONE: hier muss message gespeichert werden
    children.addElement(message);
    tag = Tag_of_Item (sk.n_g, children, root);
    vector = new Vector<BigInteger> (1);
    vector.addElement(BigInteger.ZERO);
    path.addElement (new Node (root, tag, vector, next));
  
}//Signature_Path()


//Query (private/protected):

private D Tag_of_Item (Compound n, Vector<BigInteger> children, D root) {//Vorbedingung: root.Is_in_D = true; a=Postfix_Free (children)
  //ACHTUNG: geheime Routine, verwendet Primzahlfaktoren p und q.
  //
  //                                                  -|children|
  //  -1                               root          2           (mod phi (n)/4)
  //f          (root) = (+-)(----------------------)                          (mod n)
  // children, n                 a (mod phi(n)/4)
  //                            4  
  //
  //Diese Routine berechnete den Funktionswert des Knotenobjekts tag.

  //Variablen:
  D tag;//Funktionswert tag
  BigInteger f_x, f_1, f_2, f_3, a; //Hilfsvariable
  Integer Ilength;//laenge der Postfix-freien Kodierung von childre als Integer
  int comp;//Hilfsvariable fuer BigInteger.compareTo
  BigInteger phi_n;//Eulersche Phi-Funktion. phi(n)=(p-1)(q-1)
  BigInteger phi_n_fourth;//(phi_n / 4)
  BigInteger twos_inverse;//Modulares Inverses von 2 im Restklassenring Z_phi(n)/4
  BigInteger fours_inverse;//Modulares Inverses von 4 im Restklassenring Z_n
  Integer Ichildren_length;//laenge der children als Integer
  BigInteger Bchildren_length;//...als BigInteger

  //Berechnung der Variablenwerte:
  tag = new D (n, k, std_rnd);//Vorbelegung durch Zufallswert
  a=Postfix_Free (children);//a ist die Postfix-freie kodierung der children
  Ilength= new Integer ( a.bitLength () );//laenge von a
  new BigInteger ( Ilength.toString () );
  phi_n = n.get ().subtract (n.p.get ()).subtract (n.q.get ()).add (one);//(p-1)(q-1)=n-p-q+1
  phi_n_fourth = phi_n.divide (four);//(phi_n / 4)
  twos_inverse = two.modInverse (phi_n_fourth);//2^(-1) (mod phi (n)/4)
  fours_inverse = four.modInverse (n.get ());//4^(-1) (mod n)
  Ichildren_length = new Integer ( Length (children) );
  Bchildren_length = new BigInteger ( Ichildren_length.toString() );

 
  if ( n.Jacobi_Legendre ( root.get () ) == -1 ) { f_1 = n.get ().subtract (root.get ()); } //Vorzeichen negieren bei negativem Legendre Symbol von root ueber n
  else f_1=root.get ();//Positives Legendre Symbol fuer root ueber n

  f_2 = fours_inverse.modPow ( a.mod (phi_n_fourth), n.get () );//erste Exponentiation modulo n
  f_3 = f_1.multiply (f_2).mod (n.get ());//Eingabewert root mit f_2 multiplizieren (mod n)

  f_x = f_3.modPow ( twos_inverse.modPow ( Bchildren_length, phi_n_fourth ), n.get ());//zweite Exponentiation modulo n und dritte Exponentiation modulo phi(n)/4
						
  comp = f_x.compareTo ( n.get ().add (one).divide (two) );//s. compareTo

  if ( comp < 0 ) {tag.set (f_x);}//Wenn f_x im Wertebereich ist, dann Zuweisung an tag, sonst...
  else tag.set ( n.get ().subtract (f_x) ); //Vorzeichen aendern.

  return (tag);//Ergebniswert vom Typ D

}// Tag_of_Item


private int Length (Vector<BigInteger> children) {//pre: children.isempty ==false )
  //Vector children enthaelt eine beliebige Anzahl Zahlen BigInteger.
  //!!!(Spaeter: Vector children enthaelt Verweise auf Type1_Node. 
  //Type1_Node.root enthaelt Zahl BigInteger!!!)
  //
  //Die Laenge aller Zahlen aus (den Verweisen aus) Vector children.

  //lokale Variablen:
  BigInteger child;  
  int int_child_length;
  int int_children_length = 0;//Die Laenge von allen children

  for (int i=0; (i+1) <= children.size (); i++) {//ueber alle Vectorelemente
    child = (BigInteger)children.elementAt (i);//aktuelles Kind speichern
    int_child_length = child.bitLength () ;//Laenge des Kindes als int speichern
    int_children_length = int_children_length + int_child_length;
  }//for

  return (int_children_length);//Die Laenge aller Kinder als int

}//Length

//Command (private/protected):

private void Next_Path (BigInteger message) {//Done: BigInteger message //Vorbedingung: Is_last_Signature == false 
    //Ueberschreibt den Pfad path mit neuen Knoten und message, ab dem
    //tiefsten Knoten ohne rechtes Kind.
    //Die Datei muss als Typ BigInteger mit message übergeben werden.
 
    //lokale Variablen:
    D root, tag, child0, child1;
    Vector<BigInteger> children= new Vector<BigInteger> (2);
    int next, i, search_max;
    Node node;

    //Variableninitialisierung:
    i = path.size()-4;//war: 3
    search_max = i;
    node = (Node)path.elementAt (i);
    next = node.next;
    child0 = new D ( ((BigInteger)node.children.elementAt(0)).toByteArray () );//nicht noetig
    child1 = new D ( ((BigInteger)node.children.elementAt(1)).toByteArray () );//wird neues root

    while (next==1 & i>=0) {//war: next==1 & i>=1 //ab dem search_max-ten Element bis zum ersten Element von hinten, dass next=0 enthaelt, suchen.
      i=i-1;
      node=(Node)path.elementAt(i);
      next = node.next;
      child0 = new D ( ((BigInteger)node.children.elementAt(0)).toByteArray () );//nicht noetig
      child1 = new D ( ((BigInteger)node.children.elementAt(1)).toByteArray () );//wird neues root
    }
 
    //Element i, next = 1 setzten:
      node.next = 1;
      path.setElementAt (node, i);// i <= path.size()-3 (Vorbedingung: Is_last_Signature == false)
 
  if ( i+1 <= (search_max) ) {//neuen Pfad zur nachricht message ab Position i+1
    for (int j=i+1; j <= (path.size()-3); j++) {//neue Elemente von Position i+1 bis path.size()-3 erzeugen:
      root = child1;
      next=0;
      child0 =  new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
      child1 =  new D (new Number ( sk.n_f.get().toByteArray() ), k, std_rnd);
      children = new Vector<BigInteger> (2);
      children.addElement ( child0.get () );
      children.addElement ( child1.get () );
      tag = Tag_of_Item (sk.n_f, children, root);
      path.setElementAt (new Node (root, tag, children, next), j);
    }//for
  }//if

    //Anlegen des vorletzten Knotens:
    root = child0;
    next = 0;
    child0 = new D (new Number ( sk.n_g.get().toByteArray() ), k, std_rnd);
    children = new Vector<BigInteger> (1);
    children.addElement ( child0.get () );
    tag = Tag_of_Item (sk.n_f, children, root);
    path.setElementAt (new Node (root, tag, children, next), path.size()-2);
  
    //Anlegen des letzten Knotens:
    root=child0;
    next=0;
    children=new Vector<BigInteger>(1);//bleibt 0, anstatt message
    children.addElement ( BigInteger.ZERO );
    //children.addElement ( message );
//    tag = Tag_of_Item (sk.n_g, children, root);//removed
    Vector<BigInteger> vector = new Vector<BigInteger> (1);
    vector.addElement(message);
    tag = Tag_of_Item (sk.n_g, vector, root);//Done: add tag for message
    path.setElementAt (new Node (root, tag, children, next), path.size()-1);
 
 /* }
  else {//nur vorletzten und letzten Knoten anlegen:
    //Anlegen des vorletzten Knotens:
    root = child1;
    next = 0;
    child0 = new D (new Number ( sk.n_g.get().toByteArray() ), k, std_rnd);
    children = new Vector<BigInteger> (1);
    children.addElement ( child0.get () );
    tag = Tag_of_Item (sk.n_f, children, root);
    path.setElementAt (new Node (root, tag, children, next), path.size()-2);
  
    //Anlegen des letzten Knotens:
    root=child0;
    next=0;
//   children=new Vector(1);//bleibt leer, anstatt message
//    children.addElement ( new D (message) );//soll leer bleiben
    children=new Vector<BigInteger>(1);//bleibt leer, anstatt message
    children.addElement ( BigInteger.ZERO );//soll 0 bleiben

    Vector<BigInteger> vector = new Vector<BigInteger> (1);
    vector.addElement(message);
    tag = Tag_of_Item (sk.n_g, vector, root);//Done: add tag for message
    path.setElementAt (new Node (root, tag, children, next), path.size()-1);//Done: add empty vector children
  }//else
*/
 
}//Next_Path



//Command (public):
public void Digital_Signature (BigInteger message) {//Vorbedingung: Is_last_Signature == false
    //Signaturpfad zu einer Datei.
    //Die Datei muss mit Methoden der Klasse Dokument in den Typ BigInteger
    //umgewandelt werden.

  Next_Path (message);//hier muss Is_last_Signature () == false gelten
    
}//Digital_Signature



public void Out () {
  //Ausgabe des Pfades (OHNE geheime Information des Secret_Key und ohne message)  
  for (int i=0; i<=path.size()-1; i++)  {// path.size()=b+3, alle Pfadelemente ausgeben mit 0 statt Letzem "message"=path.size()-1
    ((Node)path.elementAt(i)).Out ();
  }  
}//Out ()


//Queries (public):
public boolean Verify_Signature (Public_Key pk, BigInteger message) {//Done:BigInteger message
    //Ist der Signaturpfad fuer den oeffentlichen Schluessel pk und Nachricht message gueltig?
    //(pk ist der zum Unterschreiben benutzten geheimen Schluessel zugehoerige sk.Corresponding_pk,
    // der aus der zum Kommandozeilenparameter filename zugehoerigen Datei eingelesen wird)


  //lokale Variablen:    
  int i, comp;
  D root, tag, comp_root;
  Vector<BigInteger> children;
  Node node;
  boolean is_valid_signature = true;
  

  //ueberpruefen des letzten Knotens:
  i=path.size()-1;
  node=(Node)path.elementAt (i);
  tag=node.tag;
  root=node.root;
//  children=node.children;//entfernt
  
  
//  comp_root = Root_of_Item (new Number ( pk.n_g.get().toByteArray() ), children, tag);//entfernt
  Vector<BigInteger> vector = new Vector<BigInteger> (1);
  vector.addElement(message);
  comp_root = Root_of_Item (new Number ( pk.n_g.get().toByteArray() ), vector, tag);//Done: mit root for message

  comp = root.get().compareTo ( comp_root.get() );  
  if (comp != 0) {
    is_valid_signature = false ;
  }

  //ueberpruefen des restlichen Pfades:
  i=path.size()-2;
  node=(Node)path.elementAt (i);
  tag=node.tag;
  root=node.root;
  children=node.children;

  comp_root = Root_of_Item (new Number ( pk.n_f.get().toByteArray() ), children, tag);
  comp = root.get().compareTo ( comp_root.get() );  
  if (comp != 0) {
    is_valid_signature = false ;
  }

  while (is_valid_signature & i>=1) {
    i=i-1;
    node=(Node)path.elementAt (i);
    tag=node.tag;
    root=node.root;
    children=node.children;

    comp_root = Root_of_Item (new Number ( pk.n_f.get().toByteArray() ), children, tag);
    comp = root.get().compareTo ( comp_root.get() );  
    if (comp != 0) {
      is_valid_signature = false ;
    } 
  }  

  //vergleichen des root-Elementes des ersten Knotens ((Node)path.elementAt (0)) mit dem im
  //oeffentlichen Schluessel pk gespeicherten Elementes root1:
  comp = root.get().compareTo (pk.root1.get());
  if (comp != 0) {
    is_valid_signature = false ;
  }
  
  return (is_valid_signature);    

}//Verifiy_Signature


/*public boolean Is_Signature_for (BigInteger message) {//Done: remove
    //Ist die im Signaturpfad gespeicherte Nachricht identisch mit der 
    //Datei message?  
    //Die Datei muss mit Methoden der Klasse Dokument in den Typ BigInteger
    //umgewandelt werden.

  //lokale Variablen:
  Node node; int comp;

  node = (Node)path.elementAt(path.size()-1);//letzter Knoten
  comp = message.compareTo ((BigInteger)node.children.elementAt(0));//s. BigInteger, compareTo 

  return (comp==0);

}// Is_Signature_for
*/

public boolean Is_last_Signature () {
    //Ist der in Vector Path gespeicherte Pfad der letzte moegliche Unterschriftenpfad?

  //lokale Variablen:
  boolean is_last_path = true;
  int next, i;
  Node node;

  //durchlauf des Pfades ab dem vorvorletzten Knoten:
  i=path.size()-4; //alle internen Knoten haben zwei Kinder Done: i=path.size()-4; war: i=path.size()-3
//  node=(Node)path.elementAt (i);
//  next=node.next;//==0, immer!//falsch, denn vorvorletzter Knoten kann zwei Kinder haben!
  //vorletzter Knoten hat nur ein Kind
//  if (i>=1) {
//	  next=1;//Initialisierung jetzt in for-Schleife
//  }
  
  for (next=1; (next==1)&(i>=0); i--){
	  	node=(Node)path.elementAt(i);
	    next=node.next;
	    if (next == 0) {is_last_path=false; break;}
  }
  
//  if (i==0){
//		node=(Node)path.elementAt(i);
//	    next=node.next;
//	    if (next == 0) {is_last_path=false;}
//  }
  
//  while (next==1 & i>=1) {
//    i=i-1;
//    node=(Node)path.elementAt(i);
//    next=node.next;
//    if (next == 0) {is_last_path=false;}
//  }

  return (is_last_path);//true, wenn alle Elemente von path.size()-4 bis 0 Done: path.size()-4
    //next == 1 haben

}//Is_last_Signature


public D Root_of_Item (Number n, Vector<BigInteger> children, D tag) {//Vorbedingung: tag.Is_in_D = true; a=Postfix_Free (children)
    //                                   |children|
    //                           a      2           
    //f            (tag) = (+-) 4  * tag           (mod n)
    // children, n
    //
    //Diese Routine berechnet den Funktionswert des Knotenobjekts root.

   D root;//Funktionswert
   BigInteger f_x, f_1, f_2, a; //Hilfsvariable
   Integer Ilength;//laenge der Postfix-freien Kodierung von childre als Integer
   int comp;//Hilfsvariable fuer BigInteger.compareTo

   root = new D (n, k, std_rnd);//Vorbelegung durch Zufallswert
   a=Postfix_Free (children);//a ist die Postfix-freie kodierung der children
   Ilength= new Integer ( a.bitLength () );//laenge von a
   new BigInteger ( Ilength.toString () );

   f_1 = four.modPow ( a, n.get () );//erste Exponentiation modulo n

   f_2 = tag.get ().modPow ( two.pow ( Length (children)), n.get () );//zweite Exponentiation modulo n
 
   f_x = f_1.multiply ( f_2 );//Funktionswert...
   f_x = f_x.mod ( n.get () );//modulo n

   comp = f_x.compareTo ( n.get ().add (one).divide (two) );//s. compareTo

   if ( comp < 0 ) {root.set (f_x) ;}//Wenn f_x im Wertebereich ist, dann Zuweisung an root, sonst...
   else root.set ( n.get ().subtract (f_x) ); //Vorzeichen aendern.

   return (root);//Ergebniswert vom Typ D

}//Root_of_Item



public BigInteger Postfix_Free (Vector<BigInteger> children) {//pre: children.isempty ==false )

  //Vector children enthaelt eine beliebige Anzahl Zahlen BigInteger.
  //!!!(Spaeter: Vector children enthaelt Verweise auf Type1_Node. 
  //Type1_Node.root enthaelt Zahl BigInteger!!!)
  //Das Ergebnis der Kodierung vom Typ BigInteger hat die Eigenschaft, dass
  //keine andere Kodierung, die kuerzer ist, als Postfix der ersten
  //Kodierung vorkommen kann. Fuer zwei Kodierungen 
  //a= <an, ..., a2, a1> und b= <bn, ..., b2, b1> gilt
  //(o. B. d. A. ist die erste Kodierung nicht laenger als die Zweite):
  //a ist ein Postfix von b genau dann, wenn a1=b1, a2=b2, ..., an=bn


  //lokale Variablen:
  BigInteger child;  
  Integer child_length; //Die Laenge von child
  int int_child_length;
  BigInteger Bchild_length;  //Die Laenge von child als BigInteger
  BigInteger coded_children;

  child = (BigInteger)children.elementAt (0);//Erstes Kind speichern
  int_child_length = child.bitLength () ;//Laenge des ersten Kindes als Maximum speichern
  coded_children = child;//Alle Kinder als ein BigInteger, mit dem ersten Kind eingetragen
  
  for (int i=1; (i+1) <= children.size (); i++) {
    child = (BigInteger)children.elementAt (i); //aktuelles Kind
    if ( int_child_length < child.bitLength () ) {  int_child_length = child.bitLength () ;//Laenge des aktuellen Kindes maximal?
    }
  }
   
  //hintereinanderschreiben der Elemente in children in die selbe Zahl coded_children:
  for (int i=1; (i+1)<=children.size (); i++) {
    child = (BigInteger)children.elementAt(i);//aktuelles Kind
    coded_children = coded_children.multiply ( two.pow (int_child_length) );//um die Laenge des aktuellen Kindes nach links verschieben
    coded_children = coded_children.add (child);//mit child an niederwertigster Stelle beschreiben
  }//for i

  child_length = new Integer ( int_child_length );//maximale Laenge der Kinder
  Bchild_length = new BigInteger (child_length.toString ());//als BigInteger(fuer add. su.)
  coded_children = coded_children.multiply ( two.pow (32) );//um 32 Stellen (max. Groesse int_child_length) nach links verschoben und
  coded_children = coded_children.add ( Bchild_length );//die Laenge an den niederwertigsten Stellen eingefuegt

  //Ergebnis coded_children als BigInteger zurueckgeben:
  return (coded_children);
 
}//Postfix_Free


public BitSet to_bitset_representation() {//
	int next, i;
    Node node;
    BitSet bs=new BitSet();
    
    //Variableninitialisierung:
    i = path.size()-4;//war: i = path.size()-3;
    if (i>=0){
//    	bs = new BitSet(i);
       
    	for (int j=0;j<=i;j++){
    		node = (Node)path.elementAt (j);
    		next = node.next;
    	
    		if (next==0) {
    			bs.clear(i-j);
    		} else {
    			bs.set(i-j);
    		}
    	}	
    
    }
	
	return bs;
	
	
}


}//Signature_Path
