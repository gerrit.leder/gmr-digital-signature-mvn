package org.no_ip.leder.gmr;

import java.awt.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import javax.swing.*;
import org.no_ip.leder.gmr.SwingWorker;

import java.util.*;

/*
//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2014  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/**
* Dieser Typ wurde von einem SmartGuide generiert.
*/
class Wstart extends JFrame {
	/**
	*
	*/
	private static final long serialVersionUID = -7618745133034678941L;

	private boolean verifygeneratesigning = false;

	private JMenuItem ivj_ffnenMenuItem = null;
	private JMenuItem ivjAlles_ausw_hlenMenuItem = null;
	private JMenu ivjAnzeigenMenu = null;
	private JButton ivjAusschneidenButton = null;
	private JMenuItem ivjAusschneidenMenuItem = null;
	private JMenu ivjBearbeitenMenu = null;
	private JMenu ivjDateiMenu = null;
	private JButton ivjEinf_genButton = null;
	private JMenuItem ivjEinf_genMenuItem = null;
	IvjEventHandler ivjEventHandler = new IvjEventHandler();
	private JMenuItem ivjFeld___ber_MenuItem = null;
	private JMenuItem ivjFinden_ErsetzenMenuItem = null;
	private JMenuItem ivjFunktionsleisteMenuItem = null;
	private JMenu ivjHilfeMenu = null;
	private JMenuItem ivjHilfethemenMenuItem = null;
	private JPanel ivjJFrameContentPane = null;
	private JSeparator ivjJSeparator1 = null;
	private JSeparator ivjJSeparator2 = null;
	private JSeparator ivjJSeparator3 = null;
	private JButton ivjKopierenButton = null;
	private JMenuItem ivjKopierenMenuItem = null;
	private JMenuItem ivjL_schenMenuItem = null;
	private JMenuItem ivjNeuMenuItem = null;
	private JMenuItem ivjOnline_B_cherMenuItem = null;
	private JMenuItem ivjSichern_alsMenuItem = null;
	private JMenuItem ivjSichernMenuItem = null;
	private JPanel ivjStatusBarPane = null;
	private JMenuItem ivjStatusleisteMenuItem = null;
	private JLabel ivjStatusMsg1 = null;
	private JLabel ivjStatusMsg2 = null;
	private JToolBar ivjToolBarPane = null;
	private JMenuItem ivjVerlassenMenuItem = null;
	private JMenuItem ivjWiderruf_zur_cknehmenMenuItem = null;
	private JMenuItem ivjWiderrufenMenuItem = null;
	private JMenuBar ivjWstartJMenuBar = null;
	private JPanel ivjWstartPane = null;
	private FileDialog ivjdateiEingabe = null;
	private JLabel ivjJLabel1 = null;
	private JLabel ivjdateiname = null;
	private JButton ivjstart = null;
	static GMR gmr;
	private JTextField ivjbEingabe = null;
	private JTextField ivjkEingabe = null;
	private JLabel ivjLabel1 = null;
	private JLabel ivjLabel2 = null;
	private JLabel ivjJLabel11 = null;
	private JTextField ivjschluesselpaarEingabe = null;
	/*
	* protected Secret_Key sk = new Secret_Key();
	*/
	private javax.swing.JButton jButton = null;
	private javax.swing.JButton jButton1 = null;
	private javax.swing.JLabel jLabel = null;
	private javax.swing.JLabel jLabel12 = null;
	private javax.swing.JButton jButton2 = null;

	private FileDialog ivjdateiEingabe2 = null;

	Locale currentLocale;
      	static ResourceBundle messages;

	class IvjEventHandler implements java.awt.event.ActionListener,
	java.awt.event.ComponentListener {
		public void actionPerformed(java.awt.event.ActionEvent e) {
			if (e.getSource() == Wstart.this.getVerlassenMenuItem())
			connEtoM1(e);
	//		if (e.getSource() == Wstart.this.getFunktionsleisteMenuItem())
			connEtoC1(e);
			if (e.getSource() == Wstart.this.getStatusleisteMenuItem())
			connEtoC2(e);
			if (e.getSource() == Wstart.this.getFeld___ber_MenuItem())
			connEtoC3(e);
			if (e.getSource() == Wstart.this.get_ffnenMenuItem())
			connEtoM2(e);
			if (e.getSource() == Wstart.this.getstart())
			connEtoC5();
			if (e.getSource() == Wstart.this.getJButton())
			connEtoC6(e);
			if (e.getSource() == Wstart.this.getJButton2())
			connEtoM3(e);
			if (e.getSource() == Wstart.this.getJButton1())
			connEtoC7();
		};

		public void componentHidden(java.awt.event.ComponentEvent e) {
			if (e.getSource() == Wstart.this.getdateiEingabe()) {
				connEtoC4(e);
			} else if (e.getSource() == Wstart.this.getdateiEingabe2()) {
				connEtoC8(e);

			}
		};

		public void componentMoved(java.awt.event.ComponentEvent e) {
		};

		public void componentResized(java.awt.event.ComponentEvent e) {
		};

		public void componentShown(java.awt.event.ComponentEvent e) {
		};
	};

	/**
	* Wstart - Konstruktorkommentar.
	*/
	public Wstart() {
		super();

		currentLocale = Locale.getDefault();

		messages =
        ResourceBundle.getBundle("MessagesBundle",currentLocale);

		setMinimumSize(new Dimension(10, 10));
		setSize(new Dimension(700, 483));
		setPreferredSize(new Dimension(700, 483));
		setLocation(new Point(65, 25));
		initialize();
	}

	/**
	* Wstart - Konstruktorkommentar.
	*
	* @param title
	*            java.lang.String
	*/
	public Wstart(String title) {
		super(title);
	}

	/**
	* connEtoC1:
	* (FunktionsleisteMenuItem.action.actionPerformed(java.awt.event.
	* ActionEvent) --> Wstart.viewToolBar()V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC1(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
		//	this.viewToolBar();
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC2:
	* (StatusleisteMenuItem.action.actionPerformed(java.awt.event.ActionEvent)
	* --> Wstart.viewStatusBar()V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC2(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.viewStatusBar();
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC3:
	* (Feld___ber_MenuItem.action.actionPerformed(java.awt.event.ActionEvent)
	* --> Wstart.showAboutBox()V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC3(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.showAboutBox();
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC4:
	* (dateiEingabe.component.componentHidden(java.awt.event.ComponentEvent)
	* --> Wstart.dateiEingabe_WindowClosed(Ljava.awt.event.WindowEvent;)V)
	*
	* @param arg1
	*            java.awt.event.ComponentEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC4(java.awt.event.ComponentEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.dateiEingabe_WindowClosed(null);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC4:
	* (dateiEingabe.component.componentHidden(java.awt.event.ComponentEvent)
	* --> Wstart.dateiEingabe_WindowClosed(Ljava.awt.event.WindowEvent;)V)
	*
	* @param arg1
	*            java.awt.event.ComponentEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC8(java.awt.event.ComponentEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.dateiEingabe_WindowClosed2(null);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC5: (start.action. -->
	* Wstart.start_ActionPerformed(Ljava.awt.event.ActionEvent;)V)
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC5() {
		try {
			// user code begin {1}
			// user code end
			this.start_ActionPerformed(null);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC6: (verify.action. -->
	* Wstart.verify_ActionPerformed(Ljava.awt.event.ActionEvent;)V)
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC6(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.verify_ActionPerformed(null);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoC6: (signnext.action. -->
	* Wstart.signnext_ActionPerformed(Ljava.awt.event.ActionEvent;)V)
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoC7() {
		try {
			// user code begin {1}
			// user code end
			this.signnext_ActionPerformed(null);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoM1:
	* (VerlassenMenuItem.action.actionPerformed(java.awt.event.ActionEvent) -->
	* Wstart.dispose()V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoM1(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			this.dispose();
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoM2:
	* (_ffnenMenuItem.action.actionPerformed(java.awt.event.ActionEvent) -->
	* dateiEingabe.setVisible(true)V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoM2(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			getdateiEingabe().setVisible(true);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* connEtoM3: (Browse.action.actionPerformed(java.awt.event.ActionEvent) -->
	* dateiEingabe.setVisible(true)V)
	*
	* @param arg1
	*            java.awt.event.ActionEvent
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void connEtoM3(java.awt.event.ActionEvent arg1) {
		try {
			// user code begin {1}
			// user code end
			getdateiEingabe2().setVisible(true);
			// user code begin {2}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {3}
			// user code end
			handleException(ivjExc);
		}
	}

	/**
	* Comment
	*/
	public void dateiEingabe_WindowClosed(java.awt.event.WindowEvent windowEvent) {
		getdateiname().setText(
		getdateiEingabe().getDirectory() + getdateiEingabe().getFile());
		return;
	}

	/**
	* Comment
	*/
	public void dateiEingabe_WindowClosed2(
	java.awt.event.WindowEvent windowEvent) {
		getJLabel12().setText(
		getdateiEingabe2().getDirectory()
		+ getdateiEingabe2().getFile());
		return;
	}

	/**
	* Den Eigenschaftswert _ffnenMenuItem zurueckgeben.
	*
	* @return javax.swing.JMenuItem
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenuItem get_ffnenMenuItem() {
		if (ivj_ffnenMenuItem == null) {
			try {
				ivj_ffnenMenuItem = new javax.swing.JMenuItem();
				ivj_ffnenMenuItem.setName("_ffnenMenuItem");
				ivj_ffnenMenuItem.setText(messages.getString("Oeffnen"));
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivj_ffnenMenuItem;
	}


	/**
	* Den Eigenschaftswert AnzeigenMenu zurueckgeben.
	*
	* @return javax.swing.JMenu
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenu getAnzeigenMenu() {
		if (ivjAnzeigenMenu == null) {
			try {
				ivjAnzeigenMenu = new javax.swing.JMenu();
				ivjAnzeigenMenu.setName("AnzeigenMenu");
				ivjAnzeigenMenu.setText(messages.getString("Anzeigen"));
	//			ivjAnzeigenMenu.add(getFunktionsleisteMenuItem());
				ivjAnzeigenMenu.add(getStatusleisteMenuItem());
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjAnzeigenMenu;
	}




	/**
	* Den Eigenschaftswert bEingabe zurueckgeben.
	*
	* @return java.awt.TextField
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JTextField getbEingabe() {
		if (ivjbEingabe == null) {
			try {
				ivjbEingabe = new javax.swing.JTextField();
				ivjbEingabe.setName("bEingabe");
				ivjbEingabe.setText("10");
				ivjbEingabe.setBounds(585, 75, 46, 23);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjbEingabe;
	}

	/**
	* Den Eigenschaftswert dateiEingabe zurueckgeben.
	*
	* @return java.awt.FileDialog
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private java.awt.FileDialog getdateiEingabe() {
		if (ivjdateiEingabe == null) {
			try {
				ivjdateiEingabe = new java.awt.FileDialog(this);
				ivjdateiEingabe.setName("dateiEingabe");
				ivjdateiEingabe.setLayout(null);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjdateiEingabe;
	}

	/**
	* Den Eigenschaftswert DateiMenu zurueckgeben.
	*
	* @return javax.swing.JMenu
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenu getDateiMenu() {
		if (ivjDateiMenu == null) {
			try {
				ivjDateiMenu = new javax.swing.JMenu();
				ivjDateiMenu.setName("DateiMenu");
				ivjDateiMenu.setText(messages.getString("Datei"));
	//			ivjDateiMenu.add(getNeuMenuItem());
				ivjDateiMenu.add(get_ffnenMenuItem());
				ivjDateiMenu.add(getJSeparator1());
	//			ivjDateiMenu.add(getSichernMenuItem());
	//			ivjDateiMenu.add(getSichern_alsMenuItem());
				ivjDateiMenu.add(getVerlassenMenuItem());
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjDateiMenu;
	}

	/**
	* Den Eigenschaftswert dateiname zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getdateiname() {
		if (ivjdateiname == null) {
			try {
				ivjdateiname = new javax.swing.JLabel();
				ivjdateiname.setName("dateiname");
				ivjdateiname.setText("");
				ivjdateiname.setBounds(15, 45, 616, 23);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjdateiname;
	}



	/**
	* Den Eigenschaftswert Feld___ber_MenuItem zurueckgeben.
	*
	* @return javax.swing.JMenuItem
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenuItem getFeld___ber_MenuItem() {
		if (ivjFeld___ber_MenuItem == null) {
			try {
				ivjFeld___ber_MenuItem = new javax.swing.JMenuItem();
				ivjFeld___ber_MenuItem.setName("Feld___ber_MenuItem");
				ivjFeld___ber_MenuItem.setText(messages.getString("Ueber"));
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjFeld___ber_MenuItem;
	}



	/**
	* Den Eigenschaftswert HilfeMenu zurueckgeben.
	*
	* @return javax.swing.JMenu
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenu getHilfeMenu() {
		if (ivjHilfeMenu == null) {
			try {
				ivjHilfeMenu = new javax.swing.JMenu();
				ivjHilfeMenu.setName("HilfeMenu");
				ivjHilfeMenu.setText(messages.getString("Hilfe"));
				//ivjHilfeMenu.add(getHilfethemenMenuItem());
				//ivjHilfeMenu.add(getOnline_B_cherMenuItem());
				ivjHilfeMenu.add(getFeld___ber_MenuItem());
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjHilfeMenu;
	}


	/**
	* Den Eigenschaftswert JFrameContentPane zurueckgeben.
	*
	* @return javax.swing.JPanel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JPanel getJFrameContentPane() {
		if (ivjJFrameContentPane == null) {
			try {
				ivjJFrameContentPane = new javax.swing.JPanel();
				ivjJFrameContentPane.setName("JFrameContentPane");
				ivjJFrameContentPane.setLayout(new java.awt.BorderLayout());
	//			getJFrameContentPane().add(getToolBarPane(), "North");
				getJFrameContentPane().add(getStatusBarPane(), "South");
				getJFrameContentPane().add(getWstartPane(), "Center");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJFrameContentPane;
	}

	/**
	* Den Eigenschaftswert JLabel1 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getJLabel1() {
		if (ivjJLabel1 == null) {
			try {
				ivjJLabel1 = new javax.swing.JLabel();
				ivjJLabel1.setName("JLabel1");
				ivjJLabel1
				.setText(messages.getString("Dateiname"));
				ivjJLabel1.setBounds(15, 15, 500, 23);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJLabel1;
	}

	/**
	* Den Eigenschaftswert JLabel11 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getJLabel11() {
		if (ivjJLabel11 == null) {
			try {
				ivjJLabel11 = new javax.swing.JLabel();
				ivjJLabel11.setName("JLabel11");
				ivjJLabel11.setText(messages.getString("Schluesselpaardateien"));
				ivjJLabel11.setBounds(15, 135, 376, 23);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJLabel11;
	}

	/**
	* Den Eigenschaftswert JSeparator1 zurueckgeben.
	*
	* @return javax.swing.JSeparator
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JSeparator getJSeparator1() {
		if (ivjJSeparator1 == null) {
			try {
				ivjJSeparator1 = new javax.swing.JSeparator();
				ivjJSeparator1.setName("JSeparator1");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJSeparator1;
	}

	/**
	* Den Eigenschaftswert JSeparator2 zurueckgeben.
	*
	* @return javax.swing.JSeparator
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JSeparator getJSeparator2() {
		if (ivjJSeparator2 == null) {
			try {
				ivjJSeparator2 = new javax.swing.JSeparator();
				ivjJSeparator2.setName("JSeparator2");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJSeparator2;
	}

	/**
	* Den Eigenschaftswert JSeparator3 zurueckgeben.
	*
	* @return javax.swing.JSeparator
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JSeparator getJSeparator3() {
		if (ivjJSeparator3 == null) {
			try {
				ivjJSeparator3 = new javax.swing.JSeparator();
				ivjJSeparator3.setName("JSeparator3");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjJSeparator3;
	}

	/**
	* Den Eigenschaftswert kEingabe zurueckgeben.
	*
	* @return java.awt.TextField
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JTextField getkEingabe() {
		if (ivjkEingabe == null) {
			try {
				ivjkEingabe = new javax.swing.JTextField();
				ivjkEingabe.setName("kEingabe");
				ivjkEingabe.setText("2048");
				ivjkEingabe.setBounds(585, 105, 46, 24);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjkEingabe;
	}




	/**
	* Den Eigenschaftswert Label1 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getLabel1() {
		if (ivjLabel1 == null) {
			try {
				ivjLabel1 = new javax.swing.JLabel();
				ivjLabel1.setName("Label1");
				ivjLabel1
				.setText(messages.getString("b"));
				ivjLabel1.setBounds(15, 75, 556, 23);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjLabel1;
	}

	/**
	* Den Eigenschaftswert Label2 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getLabel2() {
		if (ivjLabel2 == null) {
			try {
				ivjLabel2 = new javax.swing.JLabel();
				ivjLabel2.setName("Label2");
				ivjLabel2
				.setText(messages.getString("k"));
				ivjLabel2.setBounds(15, 105, 575, 24);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjLabel2;
	}


	/**
	* Den Eigenschaftswert schluesselpaarEingabe zurueckgeben.
	*
	* @return java.awt.TextField
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JTextField getschluesselpaarEingabe() {
		if (ivjschluesselpaarEingabe == null) {
			try {
				ivjschluesselpaarEingabe = new javax.swing.JTextField();
				ivjschluesselpaarEingabe.setName("schluesselpaarEingabe");
				ivjschluesselpaarEingabe.setText(messages.getString("Nachname"));
				ivjschluesselpaarEingabe.setBounds(405, 135, 226, 24);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjschluesselpaarEingabe;
	}



	/**
	* Den Eigenschaftswert start zurueckgeben.
	*
	* @return java.awt.Button
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JButton getstart() {
		if (ivjstart == null) {
			try {
				ivjstart = new javax.swing.JButton(messages.getString("generate_sign"));
				ivjstart.setName("start");
				ivjstart.setBounds(465, 300, 133, 41);
				/* ivjstart.setLabel("generate&sign"); */
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjstart;
	}

	/**
	* Den Eigenschaftswert StatusBarPane zurueckgeben.
	*
	* @return javax.swing.JPanel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JPanel getStatusBarPane() {
		if (ivjStatusBarPane == null) {
			try {
				ivjStatusBarPane = new javax.swing.JPanel();
				ivjStatusBarPane.setName("StatusBarPane");
				ivjStatusBarPane.setLayout(new java.awt.BorderLayout());
				getStatusBarPane().add(getStatusMsg1(), "West");
				getStatusBarPane().add(getStatusMsg2(), "Center");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjStatusBarPane;
	}

	/**
	* Den Eigenschaftswert StatusleisteMenuItem zurueckgeben.
	*
	* @return javax.swing.JMenuItem
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenuItem getStatusleisteMenuItem() {
		if (ivjStatusleisteMenuItem == null) {
			try {
				ivjStatusleisteMenuItem = new javax.swing.JMenuItem();
				ivjStatusleisteMenuItem.setName("StatusleisteMenuItem");
				ivjStatusleisteMenuItem.setText(messages.getString("Statusleiste"));
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjStatusleisteMenuItem;
	}

	/**
	* Den Eigenschaftswert StatusMsg1 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getStatusMsg1() {
		if (ivjStatusMsg1 == null) {
			try {
				ivjStatusMsg1 = new javax.swing.JLabel();
				ivjStatusMsg1.setName("StatusMsg1");
				ivjStatusMsg1.setBorder(new javax.swing.border.EtchedBorder());
				ivjStatusMsg1.setText(messages.getString("Meldung"));
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjStatusMsg1;
	}

	/**
	* Den Eigenschaftswert StatusMsg2 zurueckgeben.
	*
	* @return javax.swing.JLabel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JLabel getStatusMsg2() {
		if (ivjStatusMsg2 == null) {
			try {
				ivjStatusMsg2 = new javax.swing.JLabel();
				ivjStatusMsg2.setName("StatusMsg2");
				ivjStatusMsg2.setBorder(new javax.swing.border.EtchedBorder());
				ivjStatusMsg2.setText("");
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjStatusMsg2;
	}


	/**
	* Den Eigenschaftswert VerlassenMenuItem zurueckgeben.
	*
	* @return javax.swing.JMenuItem
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenuItem getVerlassenMenuItem() {
		if (ivjVerlassenMenuItem == null) {
			try {
				ivjVerlassenMenuItem = new javax.swing.JMenuItem();
				ivjVerlassenMenuItem.setName("VerlassenMenuItem");
				ivjVerlassenMenuItem.setText(messages.getString("Verlassen"));
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjVerlassenMenuItem;
	}



	/**
	* Den Eigenschaftswert WstartJMenuBar zurueckgeben.
	*
	* @return javax.swing.JMenuBar
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JMenuBar getWstartJMenuBar() {
		if (ivjWstartJMenuBar == null) {
			try {
				ivjWstartJMenuBar = new javax.swing.JMenuBar();
				ivjWstartJMenuBar.setName("WstartJMenuBar");
				ivjWstartJMenuBar.add(getDateiMenu());
//				ivjWstartJMenuBar.add(getBearbeitenMenu());
				ivjWstartJMenuBar.add(getAnzeigenMenu());
				ivjWstartJMenuBar.add(getHilfeMenu());
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjWstartJMenuBar;
	}

	/**
	* Den Eigenschaftswert WstartPane zurueckgeben.
	*
	* @return javax.swing.JPanel
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private javax.swing.JPanel getWstartPane() {
		if (ivjWstartPane == null) {
			try {
				ivjWstartPane = new javax.swing.JPanel();
				ivjWstartPane.setLocation(new Point(65, 25));
				ivjWstartPane.setName("WstartPane");
				ivjWstartPane.setLayout(null);
				getWstartPane().add(getJLabel1(), getJLabel1().getName());
				getWstartPane().add(getdateiname(), getdateiname().getName());
				getWstartPane().add(getstart(), getstart().getName());
				getWstartPane().add(getLabel1(), getLabel1().getName());
				getWstartPane().add(getbEingabe(), getbEingabe().getName());
				getWstartPane().add(getLabel2(), getLabel2().getName());
				getWstartPane().add(getkEingabe(), getkEingabe().getName());
				getWstartPane().add(getJLabel11(), getJLabel11().getName());
				getWstartPane().add(getschluesselpaarEingabe(),
				getschluesselpaarEingabe().getName());
				ivjWstartPane.add(getJButton(), null);
				ivjWstartPane.add(getJButton1(), null);
				ivjWstartPane.add(getJLabel(), null);
				ivjWstartPane.add(getJLabel12(), null);
				ivjWstartPane.add(getJButton2(), null);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjWstartPane;
	}

	/**
	* Wird aufgerufen, wenn die Komponente eine Ausnahmebedingung uebergibt.
	*
	* @param exception
	*            java.lang.Throwable
	*/
	private void handleException(java.lang.Throwable exception) {

		/*
		* Entfernen Sie den Kommentar fuer die folgenden Zeilen, um nicht
		* abgefangene Ausnahmebedingungen auf der Standardausgabeeinheit
		* (stdout) auszugeben
		*/
		// System.out.println("--------- NICHT ABGEFANGENE AUSNAHMEBEDINGUNG ---------");
		// exception.printStackTrace(System.out);
	}

	/**
	* Initialisiert Verbindungen
	*
	* @exception java.lang.Exception
	*                Die Beschreibung der Ausnahmebedingung.
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void initConnections() throws java.lang.Exception {
		// user code begin {1}
		// user code end
		getVerlassenMenuItem().addActionListener(ivjEventHandler);
	//	getFunktionsleisteMenuItem().addActionListener(ivjEventHandler);
		getStatusleisteMenuItem().addActionListener(ivjEventHandler);
		getFeld___ber_MenuItem().addActionListener(ivjEventHandler);
		get_ffnenMenuItem().addActionListener(ivjEventHandler);
		getdateiEingabe().addComponentListener(ivjEventHandler);
		getdateiEingabe2().addComponentListener(ivjEventHandler);
		getstart().addActionListener(ivjEventHandler);
		getJButton().addActionListener(ivjEventHandler);
		getJButton1().addActionListener(ivjEventHandler);
		getJButton2().addActionListener(ivjEventHandler);
	}

	/**
	* Den Eigenschaftswert dateiEingabe zurueckgeben.
	*
	* @return java.awt.FileDialog
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private java.awt.FileDialog getdateiEingabe2() {
		if (ivjdateiEingabe2 == null) {
			try {
				ivjdateiEingabe2 = new java.awt.FileDialog(this);
				ivjdateiEingabe2.setName("dateiEingabe2");
				ivjdateiEingabe2.setLayout(null);
				// user code begin {1}
				// user code end
			} catch (java.lang.Throwable ivjExc) {
				// user code begin {2}
				// user code end
				handleException(ivjExc);
			}
		}
		return ivjdateiEingabe2;
	}

	/**
	* Die Klasse initialisieren.
	*/
	/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
	private void initialize() {
		try {
			// user code begin {1}
			// user code end
			setName("Wstart");
			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			setJMenuBar(getWstartJMenuBar());
			setSize(645, 483);
			setTitle("Wstart");
			setContentPane(getJFrameContentPane());
			initConnections();
		} catch (java.lang.Throwable ivjExc) {
			handleException(ivjExc);
		}
		// user code begin {2}
		// user code end
	}

	/**
	* Startet die Anwendung.
	*
	* @param args
	*            ein Array von Befehlszeilenargumenten
	*/
	public static void main(java.lang.String[] args) {
		try {
			/* Eigene Darstellungsart festlegen */
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			/* Den Rahmen erstellen */
			Wstart aWstart = new Wstart();

			/* Die Anzeigegroesse berechnen */
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

			/* Rahmen im Bildschirm stapeln */
			aWstart.pack();
			aWstart.setSize(645, 483);

			/* Rahmen im Bildschirm zentrieren */
			Dimension frameSize = aWstart.getSize();
			if (frameSize.height > screenSize.height)
			frameSize.height = screenSize.height;
			if (frameSize.width > screenSize.width)
			frameSize.width = screenSize.width;
			aWstart.setLocation((screenSize.width - frameSize.width) / 2,
			(screenSize.height - frameSize.height) / 2);

			/* Einen windowListener fuer windowClosedEvent hinzufuegen */
			aWstart.addWindowListener(new java.awt.event.WindowAdapter() {
				public void windowClosed(java.awt.event.WindowEvent e) {
					System.exit(0);
				};
			});
			aWstart.setVisible(true);
		} catch (Throwable exception) {
			//System.err.println(messages.getString("Exception"));
			exception.printStackTrace(System.out);
		}
	}

	public void showAboutBox() {
		/* Den Dialog AboutBox erstellen */
		WstartAboutBox aWstartAboutBox = new WstartAboutBox();
		Dimension dialogSize = aWstartAboutBox.getPreferredSize();
		Dimension frameSize = getSize();
		Point loc = getLocation();
		aWstartAboutBox.setLocation((frameSize.width - dialogSize.width) / 2
		+ loc.x, (frameSize.height - dialogSize.height) / 2 + loc.y);
		aWstartAboutBox.setModal(true);
		aWstartAboutBox.setVisible(true);
	}

	/**
	* Comment
	*/
	public void start_ActionPerformed(java.awt.event.ActionEvent actionEvent) {

		// falls wir bereits generieren oder signieren tun wir nichts
		if (verifygeneratesigning)
		return;
		// Semaphor auf true
		verifygeneratesigning = true;
		// Der Generiervorgang erfolgt in einem separaten Thread
		// der vom SwingWorker worker verwaltet wird.
		final SwingWorker worker = new SwingWorker() {
			public Object construct() {
				// Hier erfolgt die eigentliche Generierung.
				//try {


				try {

					BufferedReader br = new BufferedReader(new FileReader(getdateiname().getText()));


					if (getdateiname().getText().equals(""))
					getStatusMsg2().setText(messages.getString("MsgOpen"));
					else if (br.readLine() == null) {
						//							JOptionPane joptionPane = new JOptionPane();
						JOptionPane.showMessageDialog (jButton1, messages.getString("MsgEmpty1") + getdateiname().getText() + messages.getString("MsgEmpty2"));
						br.close();
					}
					else {
						getStatusMsg2().setText(messages.getString("Keygeneration"));

						String pfad = getdateiEingabe().getDirectory();
						String eineName = getdateiname().getText();
						String name = getschluesselpaarEingabe().getText();



						String filePathString = eineName + ".gmr";
						String filePathString2 = pfad + name + ".sk";;
						String filePathString3 = pfad + name + ".pk";;
						File f;
						File f2;
						File f3;

						f = new File(filePathString);
						f2 = new File(filePathString2);
						f3 = new File(filePathString3);
						if(f.exists() && f.isFile() || f2.exists() && f2.isFile() || f3.exists() && f3.isFile()) {
							//do something ...

							JOptionPane joptionPane = new JOptionPane();
							// Bsp. 4: Optionsdialog mit Warnhinweis
							int dialogButton = JOptionPane.showOptionDialog(null, messages.getString("MsgOverwrite"), messages.getString ("Overwrite"),
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.WARNING_MESSAGE, null,
							new String[]{messages.getString("Ja"), messages.getString("Nein"), messages.getString("Abbrechen")}, messages.getString("Nein"));
							if (dialogButton == JOptionPane.YES_OPTION) {
								write_gmr (eineName);
								write_sk_pk (pfad, name);
							} else {
								getStatusMsg2().setText("");

							}


						} else {
							write_gmr (eineName);

							write_sk_pk (pfad, name);


						}





					}


					//////////////
				} catch (NumberFormatException e) {
					getStatusMsg2().setText(messages.getString("Internalerror")+e);
					e.printStackTrace();
				}
				catch (Exception e) {
					getStatusMsg2().setText(messages.getString("Internalerror")+e);//Done: ArrayIndexOutOfBoundsException: 0>=0
					e.printStackTrace();
				}

				//}
				return null;
			}
		};
		worker.start();
		// Semaphor wieder zuruecksetzen.
		verifygeneratesigning = false;



		return;
	}

	/**
	* Comment
	*/
	public void verify_ActionPerformed(java.awt.event.ActionEvent actionEvent) {
		// falls wir bereits arbeiten tun wir nichts
		if (verifygeneratesigning)
		return;
		// Semaphor auf true
		verifygeneratesigning = true;
		// Der Vorgang erfolgt in einem separaten Thread
		// der vom SwingWorker worker verwaltet wird.
		final SwingWorker worker = new SwingWorker() {
			public Object construct() {
				// Hier erfolgt der eigentliche Vorgang

				try {

					BufferedReader br = new BufferedReader(new FileReader(
					getdateiname().getText()));

					if (getdateiname().getText().equals(""))
					getStatusMsg2()
					.setText(
					messages.getString("MsgOpenSignVerify"));
					else if (br.readLine() == null) {
						// JOptionPane joptionPane = new JOptionPane();
						JOptionPane
						.showMessageDialog(
						jButton1,
						messages.getString("MsgEmpty1")
						+ getdateiname().getText()
						+ messages.getString ("MsgEmpty3"));
						br.close();
					} else {
						getStatusMsg2()
						.setText(
						messages.getString("Verification"));

						String pfad = getdateiEingabe().getDirectory();
						String eineName = getdateiname().getText();
						String name = getschluesselpaarEingabe().getText();

						gmr = new GMR(eineName, eineName + ".gmr", pfad + name
						+ ".pk");
						/*
						* gmr = new
						* GMR(getdateiname().getText(),Integer.parseInt
						* (getbEingabe
						* ().getText()),Integer.parseInt(getkEingabe
						* ().getText()));
						*/
						// Initialisierungen, out:
						if (gmr.dokument.Verify()) {

							// JOptionPane joptionPane = new JOptionPane();
							JOptionPane.showMessageDialog(jButton1,
							messages.getString("MsgSignatureVerify1") + eineName
							+ messages.getString("MsgSignatureVerify2") + name + ".pk");
						} else {
							// JOptionPane joptionPane = new JOptionPane();
							JOptionPane.showMessageDialog(jButton1,
							messages.getString("MsgSignatureVerify1") + eineName
							+ messages.getString("MsgSignatureVerify3") + name + ".pk");
						}

						// ////////////

						getStatusMsg2().setText(
						eineName + messages.getString("MsgVerified"));
					}

				} catch (Exception e) {
					getStatusMsg2().setText(
					messages.getString("Internalerror") + e);
					e.printStackTrace();
				}

				return null;
			}
		};
		worker.start();
		// Semaphor wieder zuruecksetzen.
		verifygeneratesigning = false;

		return;
	}

	/**
	* Comment
	*/
	public void signnext_ActionPerformed(java.awt.event.ActionEvent actionEvent) {
		try {

			BufferedReader br = new BufferedReader(new FileReader(
			getdateiname().getText()));

			if (getdateiname().getText().equals(""))
			getStatusMsg2().setText(
			messages.getString("MsgOpen"));
			else if (br.readLine() == null) {
				// JOptionPane joptionPane = new JOptionPane();
				JOptionPane.showMessageDialog(jButton1, messages.getString("MsgEmpty1")
				+ getdateiname().getText()
				+ messages.getString("MsgEmpty2"));
				br.close();
			} else {

				final String last = getJLabel12().getText();

				if (getJLabel12().getText().equals(""))
				getStatusMsg2()
				.setText(
				messages.getString("MsgLastSignature"));
				else {

					// initialisierung in:
					gmr = new GMR(getJLabel12().getText(), last);
					//

					if (gmr.dokument.signature_path.Is_last_Signature()) {

						// JOptionPane joptionPane = new JOptionPane();
						JOptionPane
						.showMessageDialog(
						jButton1,
						messages.getString("MsgIsLastSignature1") 
						+ last
						+ messages.getString("MsgIsLastSignature2"));
					} else {



						// falls wir bereits generieren oder signieren tun wir
						// nichts
						if (verifygeneratesigning) {
							br.close();

							return;
						}

						getStatusMsg2().setText(
						messages.getString("Signing"));

						// Semaphor auf true
						verifygeneratesigning = true;
						// Der Signiervorgang erfolgt in einem separaten Thread
						// der vom SwingWorker worker verwaltet wird.
						final SwingWorker worker = new SwingWorker() {
							public Object construct() throws IOException {
								// Hier erfolgt die eigentliche Signiervorgang.

								String pfad = getdateiEingabe().getDirectory();
								String eineName = getdateiname().getText();
								String name = getschluesselpaarEingabe()
								.getText();

								try {
									gmr = new GMR(eineName, last, pfad + name
									+ ".sk", pfad + name + ".pk");
									/*
									* ^^^^last_signature_path Kein BUG!
									*/


									String filePathString = eineName + ".gmr";
									//String filePathString2 = pfad + name + ".sk";;
									//String filePathString3 = pfad + name + ".pk";;
									File f;
									//File f2;
									//File f3;

									f = new File(filePathString);
									//f2 = new File(filePathString2);
									//f3 = new File(filePathString3);
									if(f.exists() && f.isFile()) {
										//do something ...

										JOptionPane joptionPane = new JOptionPane();
										// Bsp. 4: Optionsdialog mit Warnhinweis
										int dialogButton = JOptionPane.showOptionDialog(null, messages.getString ("MsgOverwrite2"), messages.getString ("Overwrite"),
										JOptionPane.YES_NO_CANCEL_OPTION,
										JOptionPane.WARNING_MESSAGE, null,
										new String[]{messages.getString("Ja"), messages.getString("Nein"), messages.getString("Abbrechen")}, messages.getString("Nein"));
										if (dialogButton == JOptionPane.YES_OPTION) {
											write_next_gmr (eineName);
										} else {
											getStatusMsg2().setText("");

										}


									} else {
										write_next_gmr (eineName);


									}





								} catch (StreamCorruptedException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (OptionalDataException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (FileNotFoundException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (NoSuchAlgorithmException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (ClassNotFoundException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (IOException e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								} catch (Exception e) {
									// Auto-generated catch block
									getStatusMsg2()
									.setText(
									messages.getString("Internalerror") + e );
									e.printStackTrace();
								}

								return null;
							}

							private void write_next_gmr(String eineName) {
								// TODO Auto-generated method stub
								// Initialisierungen, out:
								// DEBUG:
								// gmr.dokument.signature_path.Out();
								// System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
								//
								FileOutputStream fileoutput;

								try {
									fileoutput = new FileOutputStream(eineName
									+ ".gmr");


									ObjectOutputStream objectoutput;

									objectoutput = new ObjectOutputStream(
									fileoutput);

									objectoutput
									.writeObject(gmr.dokument.signature_path);
									objectoutput.close();
									fileoutput.close();

									// ////////////

									getStatusMsg2().setText(
									getdateiname().getText()
									+ ".gmr wurde erstellt.");

								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						};
						worker.start();
						// Semaphor wieder zuruecksetzen.
						verifygeneratesigning = false;

					}
				}
			}
			// ////////////
		} catch (Exception e) {
			getStatusMsg2().setText(messages.getString("Internalerror") + e );
			e.printStackTrace();
		}
		return;
	}

	public void viewStatusBar() {
		/* Statusleiste ein- oder ausblenden */
		getStatusBarPane().setVisible(!(getStatusBarPane().isVisible()));
	}


	/**
	* This method initializes jButton
	*
	* @return javax.swing.JButton
	*/
	private javax.swing.JButton getJButton() {
		if (jButton == null) {
			jButton = new javax.swing.JButton(messages.getString("verify"));
			jButton.setBounds(270, 300, 121, 36);
		}
		return jButton;
	}

	/**
	* This method initializes jButton1
	*
	* @return javax.swing.JButton
	*/
	private javax.swing.JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new javax.swing.JButton(messages.getString("sign_next"));
			jButton1.setBounds(60, 300, 138, 36);
		}
		return jButton1;
	}

	/**
	* This method initializes jLabel
	*
	* @return javax.swing.JLabel
	*/
	private javax.swing.JLabel getJLabel() {
		if (jLabel == null) {
			jLabel = new javax.swing.JLabel();
			jLabel.setBounds(15, 210, 395, 20);
			jLabel.setText(messages.getString("FilenameLast"));
		}
		return jLabel;
	}

	/**
	* This method initializes jLabel1
	*
	* @return javax.swing.JLabel
	*/
	private javax.swing.JLabel getJLabel12() {
		if (jLabel12 == null) {
			jLabel12 = new javax.swing.JLabel();
			jLabel12.setBounds(15, 240, 461, 24);
			jLabel12.setText("");
		}
		return jLabel12;
	}

	/**
	* This method initializes jButton2
	*
	* @return javax.swing.JButton
	*/
	private javax.swing.JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new javax.swing.JButton(messages.getString("browse"));
			jButton2.setBounds(480, 225, 143, 37);
		}
		return jButton2;
	}

	private void write_gmr(String eineName) {

		try {
			gmr = new GMR(getdateiname().getText(),
			Integer.parseInt(getbEingabe().getText()),
			Integer.parseInt(getkEingabe().getText()));
		} catch (Exception e) {
			getStatusMsg2().setText(messages.getString("Internalerror") + e);
			e.printStackTrace();
		}

		// Initialisierungen, out:
		// DEBUG:
		// gmr.dokument.signature_path.Out();
		// System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
		//

		// init out:
		try {
			FileOutputStream fileoutput = new FileOutputStream(eineName
			+ ".gmr");

			ObjectOutputStream objectoutput = new ObjectOutputStream(fileoutput);

			objectoutput.writeObject(gmr.dokument.signature_path);
			objectoutput.close();
			fileoutput.close();
			// ////////////

			getStatusMsg2().setText(eineName + messages.getString("GMRcreated"));

		} catch (Exception e) {
			getStatusMsg2().setText(messages.getString("Internalerror") + e);
			e.printStackTrace();
		}
	}

	private void write_sk_pk(String pfad, String name) {
		// Initialisierungen, out:

		FileOutputStream fileoutput;
		try {
			fileoutput = new FileOutputStream(pfad + name + ".sk");

			ObjectOutputStream objectoutput = new ObjectOutputStream(fileoutput);

			/*
			* sk = gmr.dokument.signature_path.sk;//write non-transient Object
			* sk
			*/objectoutput.writeObject(gmr.dokument.signature_path.sk); // write
			// transient
			// Object
			objectoutput.close();
			fileoutput.close();
			// ////////////
			// Initialisierungen, out:
			fileoutput = new FileOutputStream(pfad + name + ".pk");
			objectoutput = new ObjectOutputStream(fileoutput);

			objectoutput.writeObject(gmr.dokument.pk);
			objectoutput.close();
			fileoutput.close();

			// JOptionPane joptionPane = new JOptionPane();
			JOptionPane.showMessageDialog(jButton1, messages.getString("KeysCreated1")
			+ name + messages.getString("KeysCreated2") + name + messages.getString("KeysCreated3")
			+ messages.getString("KeysCreated4")
			+ getbEingabe().getText() + messages.getString("KeysCreated5"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

} // @jve:visual-info decl-index=0 visual-constraint="0,0"
