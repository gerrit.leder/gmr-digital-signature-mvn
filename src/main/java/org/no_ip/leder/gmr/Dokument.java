package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2014  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import  java.math.BigInteger;
import  java.util.Random;
import  java.io.*;
import org.bouncycastle.jcajce.provider.digest.SHA3;

public class Dokument {
//In einem Dokument koennen der Hash-Wert einer Nachricht, die in einer Datei
//vorliegt, ein
//Signaturpfad und ein oeffentlicher Schluessel gespeichert werden.
//Die Nachricht wird beim erstellen eines Dokuments aus der Datei eingelesen
//und ihr Hash-Wert wird gespeichert.
//Der Signaturpfad kann neu angelegt werden und ein geheimer Schlussel wird
//erstellt. Sonst werden der letzte Signaturpfad und das Schluesselpaar
//eingelesen und es kann eine neue Signatur fuer die gespeicherte Datei erstellt
//werden. Oder es werden eine zur Datei zugehoerige Signatur und ein
//oeffentlicher Schluessel eingelesen und es kann die Signatur ueberprueft
//werden.


//Klassenattribute:
  byte[] message; //Nachricht in Datei filename
  public Signature_Path signature_path;
  public Public_Key pk;    
  
  
//Konstruktoren:
public Dokument (String filename, int b, int k, int certainty, Random std_rnd) throws java.security.NoSuchAlgorithmException {
	//Liest die Datei filename ein und speichert ihren Wert in message.
	//Legt einen neuen Signaturpfad und einen neuen geheimen Schluessel an.
	//b ist der logarithmus der Anzahl der moeglichen Unterschriften und
	//k ist der Sicherheitsparameter fuer die Anzahl der Bits der
	//verwendeten Primzahlen. certainty ist der Exponent fuer Primzahl-
	//wahrscheinlichkeiten (s. java.math.BigInteger) und std_rnd eine
	//Zufallszahlenquelle (s. java.util.Random).
	//Speichert den zum neuen geheimen Schluessel, der in signature_path.sk 
	//gespeichert ist, gehoerigen oeffentlichen Schluessel in pk ab.


  To_Byte_Array (filename);//message //verschoben
  BigInteger bigmessage = null;
  if (message.length!=0) {//verhindert Zero Length BigInteger Exception in Wstart
	  bigmessage = new BigInteger (message);
  }
  signature_path = new Signature_Path (b, k, certainty, std_rnd, bigmessage);
//  To_Byte_Array (filename);//verschoben
  Set_pk (signature_path.sk.Corresponding_PK ( b, ((Node)signature_path.path.elementAt(0)).root) );

}//Dokument(String filename, int b, int k, int certainty, Random std_rnd)


  public Dokument (String filename, String last_signature_path_name)
throws FileNotFoundException,StreamCorruptedException,IOException,OptionalDataException,ClassNotFoundException,
	java.security.NoSuchAlgorithmException {// hinzugefügt filename of last generated signature
	//Liest den letzten verwendeten Signaturpfad aus der Datei 
	//last_signature_path_name ein.  
	//(Initialisiert Signature_Path)

    //lokale Variablen:
    FileInputStream fileinput;
    ObjectInputStream objectinput;

  //Initialisierungen, in:
  fileinput = new FileInputStream (last_signature_path_name);
  objectinput = new ObjectInputStream (fileinput);

  To_Byte_Array(filename);
  
  BigInteger bigmessage = null;
  if (message.length!=0) {//verhindert Zero Length BigInteger Exception in Wstart
	  bigmessage = new BigInteger (message);
  }
  signature_path = new Signature_Path (bigmessage);//Done: new BigInteger (message)
  signature_path = (Signature_Path)objectinput.readObject();
  objectinput.close();
  fileinput.close();
  
}//Dokument (String last_signature_path_name)

  public Dokument (String filename, String last_signature_path_name, String sk_name, String pk_name)
throws FileNotFoundException,StreamCorruptedException,IOException,OptionalDataException,ClassNotFoundException,
	java.security.NoSuchAlgorithmException {
	//Liest die Datei filename ein und speichert ihren Wert in message.
	//Liest den letzten verwendeten Signaturpfad aus der Datei 
	//last_signature_path_name ein. Das Schlusselpaar ist unter 
	//sk_name/pk_name abgelegt.
    //Speichert den zum Verifizieren noetigen Schluessel in pk.

    //lokale Variablen:
    FileInputStream fileinput;
    ObjectInputStream objectinput;

  To_Byte_Array (filename);//message

  //Initialisierungen, in:
  fileinput = new FileInputStream (last_signature_path_name);
  objectinput = new ObjectInputStream (fileinput);

  //  signature_path = new Signature_Path ();//obsolete
  signature_path = (Signature_Path)objectinput.readObject();
  objectinput.close();
  fileinput.close();


  //Initialisierungen, in:
  fileinput = new FileInputStream (sk_name);
  objectinput = new ObjectInputStream (fileinput);

  //  signature_path.sk = new Signature_Path.Secret_Key (); //???
  //transient attribute:
  signature_path.sk = (Secret_Key)objectinput.readObject();
  objectinput.close();
  fileinput.close();

  //Initialisierungen, in:
  fileinput = new FileInputStream (pk_name);
  objectinput = new ObjectInputStream (fileinput);

  //  pk = new Public_Key (); //obsolete
  pk = (Public_Key)objectinput.readObject();
  objectinput.close();
  fileinput.close();

}//Dokument(String filename, String last_signature_path_name, String sk_name, String pk_name)
	

  public Dokument (String filename, String last_signature_path_name, String pk_name) 
throws FileNotFoundException,StreamCorruptedException,IOException,OptionalDataException,ClassNotFoundException,
	java.security.NoSuchAlgorithmException {
	//Liest die Datei filename ein und speichert ihren Wert in message.
	//Liest den letzten verwendeten Signaturpfad aus der Datei 
	//last_signature_path_name ein. Der oeffentliche Schluessel ist unter 
	//pk_name abgelegt und wird in pk gespeichert.

    //lokale Variablen:
    FileInputStream fileinput;
    ObjectInputStream objectinput;

  To_Byte_Array (filename);//message

  //Initialisierungen, in:
  fileinput = new FileInputStream (last_signature_path_name);
  objectinput = new ObjectInputStream (fileinput);
  
  BigInteger bigmessage = null;
  if (message.length!=0) {//verhindert Zero Length BigInteger Exception in Wstart
	  bigmessage = new BigInteger (message);
  }
  signature_path = new Signature_Path (bigmessage);//Done: new BigInteger (message)
  signature_path = (Signature_Path)objectinput.readObject();
  objectinput.close();
  fileinput.close();
  
  //Initialisierungen, in:
  fileinput = new FileInputStream (pk_name);
  objectinput = new ObjectInputStream (fileinput);

  //  pk = new Public_Key (); //???
  pk = (Public_Key)objectinput.readObject();
  objectinput.close();
  fileinput.close();
  
//signature_path.Signature_Path (last_signature_path_name);
//pk.Read (pk_name);//liest pk aus Datei filename ein

 

}//Dokument(String filename, String last_signature_path_name, String pk_name)



//Command:
  public void Set_pk (Public_Key pk) {
    //Setzt this.pk auf pk.

    this.pk = pk;
  }//Set_pk


public void Sign () {
	//Erstellt anhand des letzten Signaturpfades in signature_path den neuen
	//Signaturpfad fuer message und schreibt ihn in signature_path.


   if (!signature_path.Is_last_Signature()) {
	   BigInteger bigmessage = null;
   	   if (message.length!=0) {//verhindert Zero Length BigInteger Exception in Wstart
   		   bigmessage = new BigInteger (message);
       }
     signature_path.Digital_Signature ( bigmessage );//hinzugefügt
   }  
//   else {
//	   System.out.println ("Klasse Dokument, signature_path.Is_last_Signature == true"); //obsolete
//   }


}//Sign



//Queries:

  public Public_Key Get_pk () {
    //Liefert this.pk zurueck.

    return (this.pk);
  }//Get_pk


public boolean Verify () {
	//Ueberprueft mit dem oeffentlichen Schlussel pk, ob signature_path eine
	//gueltige Unterschrift fuer message enthaelt.

 // return ( signature_path.Is_Signature_for ( new BigInteger (message) ) && signature_path.Verify_Signature (pk) );//entfernt
  
  BigInteger bigmessage = null;
  if (message.length!=0) {//verhindert Zero Length BigInteger Exception in Wstart
	  bigmessage = new BigInteger (message);
  }	
  return (  signature_path.Verify_Signature (pk, bigmessage) );//hinzugefügt
  
}//Verifiy


public void To_Byte_Array (String filename) throws java.security.NoSuchAlgorithmException {
  //Liest die Nachricht, die in der Datei filename gespeichert ist, ein und
  //speichert sie als byte-Array in message.

  //lokale Variablen:
  byte[] data;
  byte[] digest;
  // MessageDigest hash = MessageDigest.getInstance("SHA-512");//SHA2 //entfernt
  SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest512();

  //Streams, in:
  File textFile;//Textdatei
  FileInputStream in;//Dateieingabe-Stream

  try {
    
      textFile = new File(filename);
      in = new FileInputStream(textFile);
      int size = (int)textFile.length(); // Dateilaenge
      int read = 0;    // Anzahl der gelesenen Zeichen
      data = new byte[size];      // Lesepuffer
      // Auslesen der Datei
      while (read < size)
        read += in.read(data, read, size-read);
      in.close();
      // Schreiben des Lesepuffers in Instanz von MessageDigest und speichern des Hash-Werts in message
      //hash.update (data);//entfernt
      //message=hash.digest ();//entfernt
      //message=data;//hinzugefügt

	digest = digestSHA3.digest(data);
	this.message = digest;


  }//try
  catch (IOException ex) {
    ex.printStackTrace();
  }

}//To_Byte_Array



}//Dokument
