package org.no_ip.leder.gmr;

//Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.util.Random;
import java.io.*;

class Secret_Key implements Serializable {
  //Der geheime Schluessel besteht aus zwei Zusammengesetzten Zahlen, die
  //jeweils 
  //durch Multiplikation von zwei Primzahlen entstehen. 
  //ACHTUNG: Die Primzahlen stellen die
  //geheime Information dar. Der Zugriff auf die Variablen n_f und n_g (und 
  //deren Primfaktoren) ist nur aus dem Packet Secret_Cluster moeglich.
  //Uebergabe der Variablen an Methoden von Klassen ausserhalb des Packet 
  //Secret_Cluster ist verboten.

  /**
	 * 
	 */
	private static final long serialVersionUID = -3497526867303784231L;
//Klassenattribute (GEHEIM):
  protected Compound n_f;//Zugriff auf p und q nur im Packet Secret_Cluster moeglich!
  protected Compound n_g;//Zugriff auf p und q nur im Packet Secret_Cluster moeglich!

//  public int k;//verwendete Bitlaenge der Zahlen p und q
 
  //Date Good_Thru //java.util.date?


  //Konstruktor (GEHEIM):
 protected Secret_Key (int k, int certainty, Random std_rnd) {
    this.n_f = new Compound (k, certainty, std_rnd);
    this.n_g = new Compound (k, certainty, std_rnd);
//    this.k = k;
  }

  //Queries (OEFFENTLICH):
  public Public_Key Corresponding_PK (int b, D root1){
    //Der zum aktuellen Secret_Key zugehoerige Schluessel. b ist die zu verwendende Laenge der Signaturpfade. root1 ist der Wert der ersten Wurzel im ersten Signaturpfad.

    return ( new Public_Key ( b, root1, new Number ( n_f.get().toByteArray() ), new Number( n_g.get().toByteArray() )) );

  }

protected Secret_Key (){
	this.n_f = new Compound();
	this.n_g = new Compound();
}

  //Queries (GEHEIM):
  protected void Out () {

    System.out.println ();

    System.out.println ("Secret_Key, Klassenattribute n_f (p, q ZUGRIFF VERWEIGERN!!!), n_g (P, Q ZUGRIFF VERWEIGERN!!!):");

//    System.out.println ( k );
    n_f.Out ();//ACHTUNG: Zugriff auf p und q!!!
    n_g.Out ();//ACHTUNG: Zugriff auf p und q!!!

    System.out.println ();

  }




}//Secrtet_Key
