package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.math.BigInteger;
import java.util.Random;


class Compound extends Number {
  //Beliebig lange ganze Zahlen (BigInteger), die aus zwei Primzahlen 
  //(Prime p, q) zusammengesetzt sind.
  //
  //ACHTUNG: Der Zugriff auf p und q des Secret_Key muss beschraenkt sein. 
  //D.h. die Variablen vom Typ Compound muessen protected sein,
  // damit sie nur im Secret_Cluster benutzt werden koennen!!!

  
  //Commands:
  // -Compound (int numBits, int certainty, Random rnd)
  //
  //Queries:
  // -boolean Is_Blum ()
  // -boolean Is_in_D (BigInteger y)
  // -boolean Is_in_Z (BigInteger y)
  // -boolean Is_in_Z_Star (BigInteger y)
  // -int Jacobi_Legendre (BigInteger y)
  // -Out ()
  //
  //+class invariant: get ()=p.get ()*q.get ()
  //Methode(n) auf BigInteger:



  /**
	 * 
	 */
	private static final long serialVersionUID = -644838018464545560L;
//Klassenattribute:
  //ACHTUNG: Geheime Werte p und q!!!
  Number p;//Klassenattribut, Primzahl
  Number q;//Klassenattribut, Primzahl


  //ueberschriebener Konstruktor (war Number):
  public Compound (int numBits, int certainty, Random rnd) {
    //Konstruktor fuer die Klassenattribute p, q, get ()=p.get ()*q.get ()
    //
    //Fuer die Primzahlen gilt:
    //Primzahlen, die (max.?) Laenge numBits haben
    // und mit einer Wahrscheinlichkeit 1-1/2 hoch certainty prim sind.
    //Fuer die zusammengesetzte Zahl get ()=n gilt:
    //n ist in der Menge der Blumzahlen 

    super (); // erzeugt den Wert zero
    

    p = new Number (numBits, rnd) ; // Zufallszahl der laenge k
    p.set ( p.get ().multiply (eight).add (three) );// p=3 (mod8)

    while ( p.get ().isProbablePrime (certainty) == false ) //vorbelegung der Zahl p ist keine Primzahl
    {
      p.set ( new BigInteger (numBits, rnd) ); // Zufallszahl der laenge k
      p.set ( p.get ().multiply (eight).add (three) );// p=3 (mod8)

    }//while


    q =  new Number (numBits, rnd) ; // Zufallszahl der laenge k
    q.set ( q.get ().multiply (eight).add (seven) );// q=3 (mod8)

    while ( q.get ().isProbablePrime (certainty) == false ) //vorbelegung der Zahl q ist keine Primzahl
    {
      q.set ( new BigInteger (numBits, rnd) ); // Zufallszahl der laenge k
      q.set ( q.get ().multiply (eight).add (seven) );// q=7 (mod8)

    }//while


    set ( p.get ().multiply ( q.get () ) );// value = p.get ()*q.get ()


    //Post: Is_Blum (p, q) == true;    
    //    get ()=p.get ()*q.get ()

  }//Compound(int numBits, int certainty, Random rnd) 
  
public Compound(){
	super();//erzeugt den Wert zero
	p = new Number();
	q = new Number();
}//Compound()


  //Queries:

public boolean Is_Blum () {
  //Erfuellen die Primzahlen p, q die Blumzahlbedingungen?
  //D. h. p, q sind (mit Vorraussetzung k=|p|=|q|) von der Form:
  //p=3 (mod 8) und 
  //q=7 (mod 8).
  //Dann ist die Zahl get ()=p.get ()*q.get () eine Blumzahl.


  BigInteger p_mod8 = p.get ().mod (eight);//Primzahl p im Restklassenring Modulo 8
  BigInteger q_mod8 = q.get ().mod (eight);//Primzahl q im Restklassenring Modulo 8
  boolean is_blum1; //Ergebnisvariable fuer Blumzahlbedingungen 1 fuer p und q
  boolean is_blum2; //Ergebnisvariable fuer Blumzahlbedingungen 2 fuer q und p


  is_blum1 = ((p_mod8.equals (three)) && (q_mod8.equals (seven))); //p_mod8 =3,
                                                                   //q_mod8 =7
  is_blum2 = ((p_mod8.equals (seven)) && (q_mod8.equals (three))); //p_mod8 =7,
                                                                   //q_mod8 =3

  if ( (is_blum1 == true) || (is_blum2 == true)  ) //erfuellen p, q die erste oder die zweite Blumzahlbedingung?
    return (true); //true, wenn ja
  else return (false);//false sonst

}//Is_Blum



public void Out () {
 //ACHTUNG:Zugriff auf geheime Variablen p und q!!!
 //gibt den Inhalt der Klassenattribute aus:

 System.out.println ();

 System.out.println ("Compound, Klassenattribute p (Zugriff verweigern!!!), q (Zugriff verweigern!!!), get (): ");

  System.out.println ( p.get ().toString () );
  System.out.println ( q.get ().toString () );
  System.out.println ( get ().toString () );

 System.out.println ();

}//Out




  //-----------------------
  //Methode(n) auf BigInteger:

  
  //------------------------

} //Compound
