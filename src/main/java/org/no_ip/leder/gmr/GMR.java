package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2014  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.io.*;
import java.security.SecureRandom;
//import java.security.SecureRandomSpi; //needs to be extended for Linux /dev/urandom ?
//cp. http://stackoverflow.com/questions/19119751/java-keytool-the-security-of-generated-keys-with-java-in-general
import java.security.NoSuchAlgorithmException;

public class GMR {
  //Legt die Objekte des Systems an.
  //Es gibt drei Konstruktoren. Einen fuer Schluesselerzeugung mit
  //gleichzeitigem
  //erstellen der ersten Unterschrift. Einen fuer das erstellen der naechsten
  //Unterschrift anhand der Letzten und einen fuer das verifizieren einer
  //Unterschrift.
  //Die Klasse GMR wird von der Klasse Start mit den entsprechenden Parametern
  //aufgerufen.

  //Variablen fuer das Signatursystem:
  SecureRandom std_rnd;//Zufallszahlenquelle, s. java.security.SecureRandom
  int certainty = 100;//fuer Primzahlwahrscheinlichkeiten, s. java.math.BigInteger

  //Klassenattribute:
  public Dokument dokument;

//Konstruktoren:

  public GMR (String filename, int b, int k)
    throws StreamCorruptedException,OptionalDataException,ClassNotFoundException,
    IOException,FileNotFoundException, java.security.NoSuchAlgorithmException{
  //Fuer die Schluesselerzeugung mit gleichzeitigem Erstellen der ersten Unterschrift.
  //Der Dateiname der zu unterschreibenden Datei ist filename.
  //b ist der Logarithmus der Anzahl der moeglichen Unterschriften und
  //k ist der Sicherheitsparameter fuer die Anzahl der Bits der
  //verwendeten Primzahlen.
  //Ruft den entsprechenden Konstruktor von Dokument auf.

  //Variableninitialisierung:
  try {
	std_rnd =  SecureRandom.getInstanceStrong ();
      System.out.println(std_rnd.getProvider().getName()+std_rnd.getAlgorithm().toString());

      byte[] bytes = new byte[20];
	std_rnd.nextBytes(bytes);
   } catch (NoSuchAlgorithmException e) {
				//	getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!NSAE");
					e.printStackTrace();
  }
  catch (Exception e) {
				//		getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!");
	e.printStackTrace();
  }
					
 

  dokument = new Dokument (filename, b, k, certainty, std_rnd);

//  dokument.Sign ();//Done: entfällt
}//GMR (String filename, int b, int k)


public GMR (String filename, String last_signature_path_name) 
    throws StreamCorruptedException,OptionalDataException,ClassNotFoundException,IOException,FileNotFoundException,
    java.security.NoSuchAlgorithmException{
  //Ruft den entsprechenden Konstruktor von Dokument auf.
  //Initialisiert

  //Variableninitialisierung:
  try {
	std_rnd =  SecureRandom.getInstanceStrong ();
	byte[] bytes = new byte[20];
	std_rnd.nextBytes(bytes);
   } catch (NoSuchAlgorithmException e) {
				//	getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!NSAE");
					e.printStackTrace();
  }
  catch (Exception e) {
				//		getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!");
	e.printStackTrace();
  }

  dokument = new Dokument (filename, last_signature_path_name);

  //transient attribute:
  dokument.signature_path.std_rnd = std_rnd; 
  
}//GMR(String last_signature_path_name)

public GMR (String filename, String last_signature_path_name, String sk_name, String pk_name) 
    throws StreamCorruptedException,OptionalDataException,ClassNotFoundException,IOException,FileNotFoundException,
    java.security.NoSuchAlgorithmException{
  //Fuer das Erstellen der naechsten Unterschrift anhand der Letzten und dem eigenen
  //Schlusselpaar.
  //Ruft den entsprechenden Konstruktor von Dokument auf.

  //Variableninitialisierung:
  try {
	std_rnd =  SecureRandom.getInstanceStrong ();
	byte[] bytes = new byte[20];
	std_rnd.nextBytes(bytes);
   } catch (NoSuchAlgorithmException e) {
				//	getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!NSAE");
					e.printStackTrace();
  }
  catch (Exception e) {
				//		getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!");
	e.printStackTrace();
  }

  dokument = new Dokument (filename, last_signature_path_name, sk_name, pk_name);

  //transient attribute:
  dokument.signature_path.std_rnd = std_rnd; 
  
  dokument.Sign ();//OK


}//GMR(String filename, String last_signature_path_name, String sk_name, String pk_name)  


public GMR (String filename, String last_signature_path_name, String pk_name)
    throws ClassNotFoundException, IOException,StreamCorruptedException,FileNotFoundException,OptionalDataException,
    java.security.NoSuchAlgorithmException{
  //Fuer das Ueberpruefen der letzten Signatur mit einem oeffentlichen Schluessel.
  //Ruft den entsprechenden Konstruktor von Dokument auf.

  //Variableninitialisierung:
  try {
	std_rnd =  SecureRandom.getInstanceStrong ();
	byte[] bytes = new byte[20];
	std_rnd.nextBytes(bytes);
   } catch (NoSuchAlgorithmException e) {
				//	getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!NSAE");
					e.printStackTrace();
  }
  catch (Exception e) {
				//		getStatusMsg2().setText("Ein interner Fehler ist aufgetreten!");
	e.printStackTrace();
  }


  dokument = new Dokument (filename, last_signature_path_name, pk_name);

  //transient attribute:
  dokument.signature_path.std_rnd =  std_rnd;


//  dokument.Verify ();//in Start.java
  


}//GMR(String filename, String last_signature_path_name, String pk_name)



}//GMR
