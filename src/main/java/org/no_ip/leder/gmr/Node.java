package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.util.Vector;
import java.io.*;
import java.math.BigInteger;

public class Node implements Serializable {
  //Knoten, die im Vector Path der Klasse Signature_Path gespeichert werden.


  /**
	 * 
	 */
	private static final long serialVersionUID = -2022441849776295354L;
//Klassenattribute:
  public D root;//Wurzel
  public D tag;//Label
  public Vector<BigInteger> children;//linkes Kind und rechtes Kind in einem Vector.
  public int next;//next = 0, wenn Nachfolger linkes Kind enthaehlt und
  //next=1, wenn Nachfolger rechtes Kind enthaelt.

  //Konstruktor:
  public Node (D root, D tag, Vector<BigInteger> children, int next) {
    //Belegung aller Klassenattribute:
    this.root = root;
    this.tag = tag;
    this.children = children;
    this.next = next;
  }//Node()

  //Queries:
  public void Out () {//Ausgabe der Attribute
    root.Out ();
    tag.Out ();
    for (int i=0; i<= children.size()-1; i++) {//gibt ein oder zwei Vektorelemente aus
      System.out.println ( ((BigInteger)children.elementAt (i)).toString() );
    }
    System.out.println ( next );
  }//Out()

}//Node
