package org.no_ip.leder.gmr;

///    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2014  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



import java.awt.*;

import javax.swing.*;

/**
 * Dieser Typ wurde von einem SmartGuide generiert.
 */
public class WstartAboutBox extends JDialog {

/**
	 *
	 */
	private static final long serialVersionUID = -6612410398388655261L;

	class IvjEventHandler implements java.awt.event.ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent e) {
			if (e.getSource() == WstartAboutBox.this.getOkButton())
				connEtoM1(e);
		};
	};
	private JLabel ivjAppName = null;
	private JPanel ivjButtonPane = null;
	private JLabel ivjCopyright = null;
	IvjEventHandler ivjEventHandler = new IvjEventHandler();
	private JLabel ivjIconLabel = null;
	private JPanel ivjIconPane = null;
	private JPanel ivjJDialogContentPane = null;
	private JButton ivjOkButton = null;
	private JLabel ivjSpacer = null;
	private JPanel ivjTextPane = null;
	private JLabel ivjUserName = null;
	private JLabel ivjVersion = null;
/**
 * WstartAboutBox - Konstruktorkommentar.
 */
public WstartAboutBox() {
	super();
	initialize();
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Dialog
 */
public WstartAboutBox(Dialog owner) {
	super(owner);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Dialog
 * @param title java.lang.String
 */
public WstartAboutBox(Dialog owner, String title) {
	super(owner, title);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Dialog
 * @param title java.lang.String
 * @param modal boolean
 */
public WstartAboutBox(Dialog owner, String title, boolean modal) {
	super(owner, title, modal);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Dialog
 * @param modal boolean
 */
public WstartAboutBox(Dialog owner, boolean modal) {
	super(owner, modal);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Frame
 */
public WstartAboutBox(Frame owner) {
	super(owner);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Frame
 * @param title java.lang.String
 */
public WstartAboutBox(Frame owner, String title) {
	super(owner, title);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Frame
 * @param title java.lang.String
 * @param modal boolean
 */
public WstartAboutBox(Frame owner, String title, boolean modal) {
	super(owner, title, modal);
}
/**
 * WstartAboutBox - Konstruktorkommentar.
 * @param owner java.awt.Frame
 * @param modal boolean
 */
public WstartAboutBox(Frame owner, boolean modal) {
	super(owner, modal);
}
/**
 * connEtoM1:  (OkButton.action.actionPerformed(java.awt.event.ActionEvent) --> WstartAboutBox.dispose()V)
 * @param arg1 java.awt.event.ActionEvent
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private void connEtoM1(java.awt.event.ActionEvent arg1) {
	try {
		// user code begin {1}
		// user code end
		this.dispose();
		// user code begin {2}
		// user code end
	} catch (java.lang.Throwable ivjExc) {
		// user code begin {3}
		// user code end
		handleException(ivjExc);
	}
}
/**
 * Den Eigenschaftswert AppName zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getAppName() {
	if (ivjAppName == null) {
		try {
			ivjAppName = new javax.swing.JLabel();
			ivjAppName.setName("AppName");
			ivjAppName.setText("Wstart");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjAppName;
}
/**
 * Den Eigenschaftswert ButtonPane zurueckgeben.
 * @return javax.swing.JPanel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JPanel getButtonPane() {
	if (ivjButtonPane == null) {
		try {
			ivjButtonPane = new javax.swing.JPanel();
			ivjButtonPane.setName("ButtonPane");
			ivjButtonPane.setLayout(new java.awt.FlowLayout());
			getButtonPane().add(getOkButton(), getOkButton().getName());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjButtonPane;
}
/**
 * Den Eigenschaftswert Copyright zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getCopyright() {
	if (ivjCopyright == null) {
		try {
			ivjCopyright = new JLabel();
			ivjCopyright.setName("Copyright");
			ivjCopyright.setText("(c) Copyright 2001-2020");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjCopyright;
}
/**
 * Den Eigenschaftswert IconLabel zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getIconLabel() {
	if (ivjIconLabel == null) {
		try {
			ivjIconLabel = new javax.swing.JLabel();
			ivjIconLabel.setName("IconLabel");
			ivjIconLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dukeMagnify.gif")));
			ivjIconLabel.setText("");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjIconLabel;
}
/**
 * Den Eigenschaftswert IconPane zurueckgeben.
 * @return javax.swing.JPanel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JPanel getIconPane() {
	if (ivjIconPane == null) {
		try {
			ivjIconPane = new javax.swing.JPanel();
			ivjIconPane.setName("IconPane");
			ivjIconPane.setLayout(new java.awt.FlowLayout());
			getIconPane().add(getIconLabel(), getIconLabel().getName());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjIconPane;
}
/**
 * Den Eigenschaftswert JDialogContentPane zurueckgeben.
 * @return javax.swing.JPanel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JPanel getJDialogContentPane() {
	if (ivjJDialogContentPane == null) {
		try {
			ivjJDialogContentPane = new javax.swing.JPanel();
			ivjJDialogContentPane.setName("JDialogContentPane");
			ivjJDialogContentPane.setLayout(new java.awt.BorderLayout());
			getJDialogContentPane().add(getButtonPane(), "South");
			getJDialogContentPane().add(getTextPane(), "Center");
			getJDialogContentPane().add(getIconPane(), "West");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjJDialogContentPane;
}
/**
 * Den Eigenschaftswert OkButton zurueckgeben.
 * @return javax.swing.JButton
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JButton getOkButton() {
	if (ivjOkButton == null) {
		try {
			ivjOkButton = new javax.swing.JButton();
			ivjOkButton.setName("OkButton");
			ivjOkButton.setText("OK");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjOkButton;
}
/**
 * Den Eigenschaftswert Spacer zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getSpacer() {
	if (ivjSpacer == null) {
		try {
			ivjSpacer = new javax.swing.JLabel();
			ivjSpacer.setName("Spacer");
			ivjSpacer.setText("");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjSpacer;
}
/**
 * Den Eigenschaftswert TextPane zurueckgeben.
 * @return javax.swing.JPanel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JPanel getTextPane() {
	if (ivjTextPane == null) {
		try {
			ivjTextPane = new javax.swing.JPanel();
			ivjTextPane.setName("TextPane");
			ivjTextPane.setLayout(getTextPaneGridLayout());
			getTextPane().add(getAppName(), getAppName().getName());
			getTextPane().add(getVersion(), getVersion().getName());
			getTextPane().add(getSpacer(), getSpacer().getName());
			getTextPane().add(getCopyright(), getCopyright().getName());
			getTextPane().add(getUserName(), getUserName().getName());
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjTextPane;
}
/**
 * Den Eigenschaftswert TextPaneGridLayout zurueckgeben.
 * @return java.awt.GridLayout
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private java.awt.GridLayout getTextPaneGridLayout() {
	java.awt.GridLayout ivjTextPaneGridLayout = null;
	try {
		/* Komponente erstellen */
		ivjTextPaneGridLayout = new java.awt.GridLayout(5, 1);
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	};
	return ivjTextPaneGridLayout;
}
/**
 * Den Eigenschaftswert UserName zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getUserName() {
	if (ivjUserName == null) {
		try {
			ivjUserName = new javax.swing.JLabel();
			ivjUserName.setName("UserName");
			ivjUserName.setText("Gerrit Leder (see CONTRIBUTE.TXT)");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjUserName;
}
/**
 * Den Eigenschaftswert Version zurueckgeben.
 * @return javax.swing.JLabel
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private javax.swing.JLabel getVersion() {
	if (ivjVersion == null) {
		try {
			ivjVersion = new javax.swing.JLabel();
			ivjVersion.setName("Version");
			ivjVersion.setText("Version 2.0.0 (see license.gpl, HISTORY.TXT)");
			// user code begin {1}
			// user code end
		} catch (java.lang.Throwable ivjExc) {
			// user code begin {2}
			// user code end
			handleException(ivjExc);
		}
	}
	return ivjVersion;
}
/**
 * Wird aufgerufen, wenn die Komponente eine Ausnahmebedingung uebergibt.
 * @param exception java.lang.Throwable
 */
private void handleException(java.lang.Throwable exception) {

	/* Entfernen Sie den Kommentar fuer die folgenden Zeilen, um nicht abgefangene Ausnahmebedingungen auf der Standardausgabeeinheit (stdout) auszugeben */
	// System.out.println("--------- NICHT ABGEFANGENE AUSNAHMEBEDINGUNG ---------");
	// exception.printStackTrace(System.out);
}
/**
 * Initialisiert Verbindungen
 * @exception java.lang.Exception Die Beschreibung der Ausnahmebedingung.
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private void initConnections() throws java.lang.Exception {
	// user code begin {1}
	// user code end
	getOkButton().addActionListener(ivjEventHandler);
}
/**
 * Die Klasse initialisieren.
 */
/* WARNUNG: DIESE METHODE WIRD ERNEUT GENERIERT. */
private void initialize() {
	try {
		// user code begin {1}
		// user code end
		setName("WstartAboutBox");
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setSize(330, 160);
		setTitle("WstartAboutBox");
		setContentPane(getJDialogContentPane());
		initConnections();
	} catch (java.lang.Throwable ivjExc) {
		handleException(ivjExc);
	}
	// user code begin {2}
	// user code end
}
/**
 * Haupteingangspunkt - Startet die Komponente, wenn sie als Anwendung ausgefuehrt wird
 * @param args java.lang.String[]
 */
public static void main(java.lang.String[] args) {
	try {
		WstartAboutBox aWstartAboutBox;
		aWstartAboutBox = new WstartAboutBox();
		aWstartAboutBox.setModal(true);
		aWstartAboutBox.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				System.exit(0);
			};
		});
		aWstartAboutBox.setVisible(true);
		java.awt.Insets insets = aWstartAboutBox.getInsets();
		aWstartAboutBox.setSize(aWstartAboutBox.getWidth() + insets.left + insets.right, aWstartAboutBox.getHeight() + insets.top + insets.bottom);
		aWstartAboutBox.setVisible(true);
	} catch (Throwable exception) {
		System.err.println("In main() von javax.swing.JDialog trat eine Ausnahmebedingung auf");
		exception.printStackTrace(System.out);
	}
}
}
