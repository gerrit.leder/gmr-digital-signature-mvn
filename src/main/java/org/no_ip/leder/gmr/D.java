package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.math.BigInteger;
import java.util.Random;


class D extends Number {
  //Beliebig lange ganze Zahlen (BigInteger) aus dem Wertebereich 0<=(n/2)+1,
  //die Positives Jakobi_Legendre Symbol bezueglich n haben.
  //(n ist die zusammengesetzte Zahl des oeffentlichen Schluessels als BigInteger,
  //ohne Primfaktoren p und q).
  //
  //Commands:
  // -D (Number n, int numBits, Random rnd)
  //  ACHTUNG:  Der Uebergabeparameter aus einer Klasse im Secret_Cluster
  //  (Signature_Path) an eine Routine einer Klasse die ungeschuetzt ist (D),
  //  darf nicht die Primzahlfaktoren der Variable n vom Typ Compound enthalten!!!
  // -D (byte[] val)
  // -D (String s)
  //
  //
  //Queries:
  // -boolean Is_in_D (BigInteger y)
  // -boolean Is_in_Z (BigInteger y)
  // -boolean Is_in_Z_Star (BigInteger y)
  // -int Jacobi_Legendre (BigInteger y)
  // -Out ()
  

  //Konstruktoren:
    
  /**
	 * 
	 */
	private static final long serialVersionUID = 2541097148704633542L;

public D (Number n, int numBits, Random rnd) {
    //Zufallszahl der Laenge numBits mit positivem Jakobisymbol bezueglich n.
    //Zufallsquelle rnd.
    //ACHTUNG:  Der Uebergabeparameter aus einer Klasse im Secret_Cluster
    //(Signature_Path) an eine Routine einer Klasse die ungeschuetzt ist (D),
    //darf nicht die Primzahlfaktoren der Variable n vom Typ Compound enthalten!!!

  super (numBits, rnd); //Zufallszahl
        
  while ( Is_in_D ( n ) == false) {
  
    set ( new BigInteger (numBits, rnd) );
  
  }//while        
       
      
  //post: Is_in_D (n) = true;
  }//D(int numBits, Random rnd) 


  public D (byte[] val) {
    //vgl. Konstruktor BigInteger

     value = new BigInteger (val); 
  }//D (byte[] val) 


  public D (String s) {
  //vgl. Konstruktor BigInteger

  value = new BigInteger (s);
  }//D (String s)

  
  public D (BigInteger i) {
	  //vgl. Konstruktor BigInteger

	  value = i;
	  }//D (String s)

  //Queries:

  public boolean Is_in_D (Number n) {
    //Vorbedingung ist, dass (Is_in_Z (y)==true) ist. (y=get ()):
    //Prueft, ob 0<y<n und n.get ().Jacobi (y) = +1.

   int upper;//Wenn y kleiner als (n.get ()/2)+1 ist, dann ist upper>0
    int lower;//Wenn y groesser 0 ist, dann ist lower<0

    upper = (n.get ().divide(two).add (one)).compareTo ( get () );//Obere Grenze
    lower = zero.compareTo ( get () );//Untere Grenze

    if ( !((upper>0) && (lower<0)) ) {return (false);} else
      {
	if ( n.Jacobi_Legendre ( get () ) == 1) return true; else return false;//Jacobi_Legendre-Symbol y ueber get () = +1
      }
 
  }//Is_in_D (BigInteger n)

  public void Out () {
  //gibt den Inhalt der Klassenattribute aus:

  System.out.println ();

  System.out.println ("D, Klassenattribute get (): ");
  System.out.println ( get ().toString () );

  System.out.println ();

}//Out

  
}//D
