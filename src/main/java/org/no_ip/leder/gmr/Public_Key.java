package org.no_ip.leder.gmr;

//Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.io.*;

class Public_Key implements Serializable {
  //Zwei der vier Primzahlen aus dem privaten Schluessel werden multipliziert
  //und als zwei BigInteger n und m gespeichert.
  //Zusaetzlich wird die oberste Wurzel root1 des Signaturpfades und die 
  //Werte k (Stellen Primzahllaengen) und b (Laenge des Signaturpfades)
  //eingetragen.


  /**
	 * 
	 */
	private static final long serialVersionUID = 2391058236581298713L;
//Klassenattribute:
public Number n_f;
public Number n_g;

public D root1;

//public int k;
public int b;

//Date Good_Thru //java.util.date?


  //Konstruktor:
  public Public_Key (int b, D root1, Number n_f, Number n_g) {
    this.n_f = n_f;
    this.n_g = n_g;
//    this.k = k;
    this.b = b;
    this.root1 = root1;
  }

  //Queries:
  public void Out () {

    System.out.println ();

    System.out.println ("Public_Key, Klassenattribute  b, root1, n_f, n_g:");

//    System.out.println ( k );
    System.out.println ( b );
    root1.Out ();
    n_f.Out();
    n_g.Out();
    //System.out.println ( n_f.toString () );
    //System.out.println ( n_g.toString () );
           
    System.out.println ();

  }


}//Public_Key
