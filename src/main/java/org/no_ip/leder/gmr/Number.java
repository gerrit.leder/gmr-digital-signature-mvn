package org.no_ip.leder.gmr;

//    Digital Signature System after Goldwasser, Micali and Rivest (GMR), Copyright (C) 2001-2013  Gerrit Leder

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


import java.io.*;
import java.math.BigInteger;
import java.util.Random;


class Number implements Serializable {
  /**
	 * 
	 */
	
	private static final long serialVersionUID = -7978986279143181641L;


//Beliebig lange ganze Zahl (BigInteger), die mit den Methoden get und set     
  //ausgelesen und ueberschrieben werden kann.
  //Elternklasse fuer Prime, Compound und D.
  //
  //Commands:
  //   -Number (byte[] val)
  //   -Number (int numBits, Random rnd)
  //   -Number (int numBits, int certainty, Random rnd)
  //   -Number (String s)
  //   -Number ()
  //   -set (BigInteger value)
  //
  //
  //Queries:
  // -toByteArray ()
  // -get()
  // -boolean Is_in_D (BigInteger y)
  // -boolean Is_in_Z (BigInteger y)
  // -boolean Is_in_Z_Star (BigInteger y)
  // -int Jacobi_Legendre (BigInteger y)
  // -Out ()
  //
  //Methode(n) auf BigInteger:
  // -calcJacobi(BigInteger a, BigInteger p) //(Author: Matthias Kolberg)
  // *BigInteger Mod_Root2 (BigInteger m) //(Author: Matthias Kolberg)



  /**
	 * 
	 */



//Klassenattribute:
  protected BigInteger value; //Klassenattribut, der Wert der repraesentierten Zahl
 

  //Konstante Zahlen:
  static final BigInteger _one = new BigInteger ("-1");
  static final BigInteger zero = new BigInteger ("0");
  static final BigInteger one = new BigInteger ("1");
  static final BigInteger two = new BigInteger ("2");
  static final BigInteger three = new BigInteger ("3");
  static final BigInteger four = new BigInteger ("4");
  static final BigInteger seven = new BigInteger ("7");
  static final BigInteger eight = new BigInteger ("8");

  

  //Konstruktoren:
  public Number () {
    //Die Zahl mit dem Wert zero:

     value = new BigInteger ("0"); 

  }//Number() 


  public Number (byte[] val) {
    //vgl. Konstruktor BigInteger

     value = new BigInteger (val); 
  }//Number(byte[] val) 



   public Number (int numBits, Random rnd) {
    //vgl. Konstruktor BigInteger

     value = new BigInteger (numBits, rnd); // Zufallszahl der laenge numbits

  }//Number(int numBits, Random rnd) 



  public Number (int numBits, int certainty, Random rnd) {
    //vgl. Konstruktor BigInteger

     value = new BigInteger (numBits, certainty, rnd); // Primzahl der laenge numbits, Primzahlwahrscheinlichkeit 1-1/2 hoch certainty,

  }//Number(int numBits, int certainty, Random rnd) 

  

  public Number (String s) {
  //vgl. Konstruktor BigInteger

  value = new BigInteger (s);
  }//Number (String s)




  //Commands:


  public void set (BigInteger value) {
	//neuer Wert fuer die repraesentierte Zahl
  
  this.value = value;

  }//set (BigInteger value)




  //Queries:

  public byte[] toByteArray (){
  //Die Zweier-Komplementdarstellung vom repraesentierten Wert der Klasse als
  //byte-Array

  return ( value.toByteArray () );

  }//toByteArray ()


  public BigInteger get () {
	//der Wert der repraesentierten Zahl
  
  return (value);

  }//get ()



  public boolean Is_in_Z (BigInteger y) {
    //Z = {0, 1, ..., n-1}, n=get ().

    int upper;//Wenn y kleiner als get () ist, dann ist upper>0
    int lower;//Wenn y groesser oder gleich 0 ist, dann ist lower<0

    upper = get ().compareTo (y);//Obere Grenze
    lower = _one.compareTo (y);//Untere Grenze

    if ( (upper>0) && (lower<0) ) {return (true);} else return (false);
  }


  public boolean Is_in_Z_Star (BigInteger y) {
    //Is_in_Z (y) == true und
    //haben y und get ()=n keinen gemeinsamen Teiler (ist gcd(n, y)=1).

    if ( !Is_in_Z (y) ) return false; else 

      if ( get ().gcd (y).equals (one) ) {return (true);} else 
        return (false);
  }





  public int Jacobi_Legendre (BigInteger y) {
    //Das Jakobi_Legendre-Symbol fuer die Zahlen y und get ()=n:
    // y     y   y
    //(-) = (-) (-)
    // n     p   q

  

    if ( !Is_in_Z_Star (y) ) return 0;
    return calcJacobi (y, get ());//Jacobisymbol y ueber get () (MK)

  }


public void Out () {
 //gibt den Inhalt der Klassenattribute aus:

  System.out.println ();

  System.out.println ("Number, Klassenattribute get (): ");
  System.out.println ( get ().toString () );

  System.out.println ();

}//Out




  //-----------------------
  //Methode(n) auf BigInteger:

  private int calcJacobi(BigInteger a, BigInteger p){
    //Jakobisymbol a ueber p (MK)
    a = a.mod(p);

    int countSplits = 0, factor,
        p_1dev2mod2     = p.subtract(one).divide(two).mod(two).intValue(),
        ppow2_1dev4mod2 = p.pow(2).subtract(one).divide(eight).mod(two).intValue();

    while (a.mod(two).intValue() == 0) {  // a even
      if (a.equals(p.add(_one))) break;
      if (a.equals(two)) break;

      a = a.divide(two);
      countSplits++;
    }
    if ((countSplits % 2) == 0) factor = +1;
    else {
      if (ppow2_1dev4mod2 == 0) factor = +1;
      else                      factor = -1;
    }
    

    if (a.equals(p.add(_one)))  // Jacobi(-1, p)
      if (p_1dev2mod2 == 0) return +1 * factor;
      else                  return -1 * factor;
    if (a.equals(one))          // Jacobi(1, p)
      return +1 * factor;
    if (a.equals(two))          // Jacobi(2, p)
      if (ppow2_1dev4mod2 == 0) return +1 * factor;
      else                      return -1 * factor;

    if (a.subtract(one).multiply(p.subtract(one)).divide(four).mod(two).intValue() == 0) factor *= +1;
    else  factor *= -1;

    return factor * calcJacobi(p, a);
  }


  public BigInteger Mod_Root2 (BigInteger m) {
    //berechnet die Modulare Quadratwurzel von get ()=n bezueglich m
    //(MK)

    //DUMMY:
    return (zero);//
    /////////////////
 
  }

  //------------------------

} //Number
