package org.no_ip.leder.gmr;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.BitSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.no_ip.leder.gmr.GMR;

public class GMRTest {

	static org.no_ip.leder.gmr.GMR gmr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		 System.out.println("TestCase setup");

			String eineName = "test2";
			String name = "Nachname";
			String pfad = "";


		 try{
	    		gmr = new GMR(eineName, 10, 2048);

	    		write_signature_keys(eineName, name, pfad);

		 }catch (Exception ex){
	    		ex.printStackTrace();
		 }
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_pos_verify_test2_sign_next_test3() {

		String eineName = "test3";
		String name = "Nachname";
		String pfad = "";
		String last = "test2.gmr";

		Boolean ex_result_ver = true;
		assertEquals("gmr.dokument.Verify(): ", ex_result_ver, gmr.dokument.Verify());


		for (org.no_ip.leder.gmr.Node node : gmr.dokument.signature_path.path) {
			assertEquals("gmr.dokument.signature_path.path: node.next==0", (Integer)0, (Integer)node.next);
		}

		BitSet ex_result = createFromString("");
		assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, gmr.dokument.signature_path.to_bitset_representation());


		assertEquals("gmr.dokument.signature_path: Is_last_Signature()==false", (Boolean)false, (Boolean)gmr.dokument.signature_path.Is_last_Signature());

		 try{
			 	gmr = new GMR(eineName, last, name+".sk", name+".pk");

	    		write_signature(eineName);

	    	}catch (Exception ex){
	    		ex.printStackTrace();
	    	}

		ex_result_ver = true;
		assertEquals("gmr.dokument.Verify(): ", ex_result_ver, gmr.dokument.Verify());


		ex_result = createFromString("1");
		assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, gmr.dokument.signature_path.to_bitset_representation());


		assertEquals("gmr.dokument.signature_path: Is_last_Signature()==false", (Boolean)false, (Boolean)gmr.dokument.signature_path.Is_last_Signature());


		eineName = "test4";
		name = "Nachname";
		pfad = "";
		last = "test3.gmr";

		 try{

			 	gmr = new GMR(eineName, last, name+".sk", name+".pk");

	    		write_signature(eineName);

	    	}catch (Exception ex){
	    		ex.printStackTrace();
	    	}


		ex_result_ver = true;
		assertEquals("gmr.dokument.Verify(): ", ex_result_ver, gmr.dokument.Verify());



		ex_result = createFromString("10");
		assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, gmr.dokument.signature_path.to_bitset_representation());


		assertEquals("gmr.dokument.signature_path: Is_last_Signature()==false", (Boolean)false, (Boolean)gmr.dokument.signature_path.Is_last_Signature());

		eineName = "test5";
		name = "Nachname";
		pfad = "";
		last = "test4.gmr";

		 try{
			 	gmr = new GMR(eineName, last, name+".sk", name+".pk");

	    		write_signature(eineName);

	    	}catch (Exception ex){
	    		ex.printStackTrace();
	    	}


		ex_result_ver = true;
		assertEquals("gmr.dokument.Verify(): ", ex_result_ver, gmr.dokument.Verify());



		ex_result = createFromString("11");
		assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, gmr.dokument.signature_path.to_bitset_representation());


		assertEquals("gmr.dokument.signature_path: Is_last_Signature()==false", (Boolean)false, (Boolean)gmr.dokument.signature_path.Is_last_Signature());

	}


	private void write_signature(String eineName) throws FileNotFoundException,
			IOException {
		//Initialisierungen, out:
		//DEBUG:
//	    		gmr.dokument.signature_path.Out();
//	    		System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
		//
		FileOutputStream fileoutput;

		fileoutput = new FileOutputStream(eineName + ".gmr");

		ObjectOutputStream objectoutput;

		objectoutput = new ObjectOutputStream(fileoutput);


		objectoutput.writeObject(gmr.dokument.signature_path);
		objectoutput.close();
		fileoutput.close();
		//////////////
	}

	private static BitSet createFromString(String s) {
	    BitSet t = new BitSet(s.length());
	    int lastBitIndex = s.length() - 1;
	    int i = lastBitIndex;
	    while ( i >= 0) {
	        if ( s.charAt(i) == '1'){
	            t.set(lastBitIndex - i);
	            i--;
	        }
	        else
	            i--;
	    }
	    return t;
	}


	private static void write_signature_keys(String eineName, String name,
			String pfad) throws FileNotFoundException, IOException {
		//Initialisierungen, out:
		//DEBUG:
//				gmr.dokument.signature_path.Out();
//				System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
		//
		FileOutputStream fileoutput = new FileOutputStream(eineName + ".gmr");
		ObjectOutputStream objectoutput = new ObjectOutputStream(fileoutput);

		objectoutput.writeObject(gmr.dokument.signature_path);
		objectoutput.close();
		fileoutput.close();
		//////////////


//				getStatusMsg2().setText(eineName + ".gmr wurde erstellt.");
		//Initialisierungen, out:

		fileoutput = new FileOutputStream(pfad + name + ".sk");
		objectoutput = new ObjectOutputStream(fileoutput);

/*		sk = gmr.dokument.signature_path.sk;//write non-transient Object sk
*/		objectoutput.writeObject(gmr.dokument.signature_path.sk); //write transient Object
		objectoutput.close();
		fileoutput.close();
		//////////////
				//Initialisierungen, out:
		fileoutput = new FileOutputStream(pfad + name + ".pk");
		objectoutput = new ObjectOutputStream(fileoutput);

		objectoutput.writeObject(gmr.dokument.pk);
		objectoutput.close();
		fileoutput.close();
	}

}
