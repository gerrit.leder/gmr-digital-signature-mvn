package org.no_ip.leder.gmr;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.File;
import java.util.BitSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.no_ip.leder.gmr.GMR;

public class GMRTest30 {

	static org.no_ip.leder.gmr.GMR gmr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		 System.out.println("TestCase setup");

			String eineName = "test6";
			String name = "Nachname";
			String new_name = "Nachname_old";
			String pfad = "";


		 try{
	    		gmr = new GMR(eineName, 1, 2048);

	    		write_signature_keys(eineName, name, pfad);

					File file = new File (name + ".sk");
					File file2 = new File (new_name + ".sk");

					// Rename file (or directory)
					boolean success = file.renameTo(file2);

					assertEquals ("File was not successfully renamed", true, success);

	    	}catch (Exception ex){
	    		ex.printStackTrace();
	    	}

	}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_sk_FNFE() {
//		fail("Not yet implemented"); //
		String eineName = "test7";
		String name = "Nachname";
		String pfad = "";
		String last = "test6.gmr";
		boolean result;
		boolean FNFE = false;
		BitSet result_bs;
		BitSet ex_result = createFromString("");
		assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, gmr.dokument.signature_path.to_bitset_representation());


		assertEquals("gmr.dokument.signature_path: Is_last_Signature()==false", false, gmr.dokument.signature_path.Is_last_Signature());

		 try{
	    		gmr = new GMR(eineName, last, name+".sk", name+".pk");

	    		write_signature(eineName);

				} catch (FileNotFoundException ex) {

					//ex.printStackTrace();

					FNFE = true;



	    	}catch (Exception ex){
	    		ex.printStackTrace();
	    	}

				assertEquals("FileNotFoundException not raised", true, FNFE);

		// ex_result = createFromString("1");
		// result_bs = gmr.dokument.signature_path.to_bitset_representation();
		// assertEquals("gmr.dokument.signature_path.to_bitset_representation(): ", ex_result, result_bs);
		//
		//
		// result=(Boolean)gmr.dokument.signature_path.Is_last_Signature();
		// assertEquals("gmr.dokument.signature_path: Is_last_Signature()==true", (Boolean)true, result);



	}


	private void write_signature(String eineName) throws FileNotFoundException,
			IOException {
		//Initialisierungen, out:
		//DEBUG:
//	    		gmr.dokument.signature_path.Out();
//	    		System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
		//
		FileOutputStream fileoutput;

		fileoutput = new FileOutputStream(eineName + ".gmr");

		ObjectOutputStream objectoutput;

		objectoutput = new ObjectOutputStream(fileoutput);


		objectoutput.writeObject(gmr.dokument.signature_path);
		objectoutput.close();
		fileoutput.close();
		//////////////
	}

	private static BitSet createFromString(String s) {
	    BitSet t = new BitSet(s.length());
	    int lastBitIndex = s.length() - 1;
	    int i = lastBitIndex;
	    while ( i >= 0) {
	        if ( s.charAt(i) == '1'){
	            t.set(lastBitIndex - i);
	            i--;
	        }
	        else
	            i--;
	    }
	    return t;
	}


	private static void write_signature_keys(String eineName, String name,
			String pfad) throws FileNotFoundException, IOException {
		//Initialisierungen, out:
		//DEBUG:
//				gmr.dokument.signature_path.Out();
//				System.out.println((gmr.dokument.signature_path.to_bitset_representation()).toString());
		//
		FileOutputStream fileoutput = new FileOutputStream(eineName + ".gmr");
		ObjectOutputStream objectoutput = new ObjectOutputStream(fileoutput);

		objectoutput.writeObject(gmr.dokument.signature_path);
		objectoutput.close();
		fileoutput.close();
		//////////////


//				getStatusMsg2().setText(eineName + ".gmr wurde erstellt.");
		//Initialisierungen, out:

		fileoutput = new FileOutputStream(pfad + name + ".sk");
		objectoutput = new ObjectOutputStream(fileoutput);

/*		sk = gmr.dokument.signature_path.sk;//write non-transient Object sk
*/		objectoutput.writeObject(gmr.dokument.signature_path.sk); //write transient Object
		objectoutput.close();
		fileoutput.close();
		//////////////
				//Initialisierungen, out:
		fileoutput = new FileOutputStream(pfad + name + ".pk");
		objectoutput = new ObjectOutputStream(fileoutput);

		objectoutput.writeObject(gmr.dokument.pk);
		objectoutput.close();
		fileoutput.close();
	}


}
