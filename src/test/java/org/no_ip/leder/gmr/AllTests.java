package org.no_ip.leder.gmr;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import org.no_ip.leder.gmr.GMR;

@RunWith(Suite.class)
@SuiteClasses({ GMRTest.class,
				GMRTest2.class,
				GMRTest3.class,
			  GMRTest30.class})
public class AllTests {



	@BeforeClass
    public static void setUpClass() {
        System.out.println("Master setup");

	}

    @AfterClass public static void tearDownClass() {
        System.out.println("Master tearDown");
    }

}
